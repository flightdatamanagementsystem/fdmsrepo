<?php
/* Kristin Hamilton
 * created 25-Apr-2016
 */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

function getFlightID($flightNumber)
{
    $flightID = -1;
    $query = "SELECT flightID FROM Flight WHERE flightNumber = $flightNumber";
    $db = dbConnect();
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain flightID<br />");
    if($result) $flightID = mysqli_fetch_row($result)[0];
    mysqli_close($db);
    return $flightID;
}

function getFlightNumber($flightID)
{
    $flightNumber = -1;
    $query = "SELECT flightNumber FROM Flight WHERE flightID = $flightID";
    $db = dbConnect();
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain flightNumber<br />");
    if($result) $flightNumber = mysqli_fetch_row($result)[0];
    mysqli_close($db);
    return $flightNumber;
}

function getFlightInfo($flightID)
{
    $flightInfo = array(
        'flightID'=>-1,
        'flightNumber'=>-1,
        'price'=>0.0,
        'aircraftRegistration'=>'',
        'modelName'=>'',
        'departureInfo'=>array(
            'date'=>'',
            'time'=>'',
            'airport'=>array(
                'id'=>-1,
                'code'=>'',
                'name'=>'',
                'address'=>'',
                'city'=>'',
                'state'=>'',
                'zip'=>-1
            )
        ),
        'arrivalInfo'=>array(
            'date'=>'',
            'time'=>'',
            'airport'=>array(
                'id'=>-1,
                'code'=>'',
                'name'=>'',
                'address'=>'',
                'city'=>'',
                'state'=>'',
                'zip'=>-1
            )
        )
    );
    
    $flightInfo['flightID'] = $flightID;
    $flightInfo['flightNumber'] = getFlightNumber($flightID);    
    $flightFields = getFlightFields($flightID);
    $flightInfo['price'] = $flightFields['price'];
    $aircraftID = $flightFields['aircraftID'];
    $departureID = $flightFields['departureID'];
    $arrivalID = $flightFields['arrivalID'];
    
    $aircraftFields = getAircraftFields($aircraftID);
    $flightInfo['aircraftRegistration'] = $aircraftFields['aircraftRegistration'];
    $flightInfo['modelName'] = $aircraftFields['modelName'];
    
    $flightInfo['departureInfo'] = getDepartureInfo($departureID);
    $flightInfo['arrivalInfo'] = getArrivalInfo($arrivalID);
    
    return $flightInfo;
}

function getFlightFields($flightID)
{
    $flightFields = array(
        'flightNumber'=>-1,
        'price'=>0.0,
        'aircraftID'=>-1,
        'departureID'=>-1,
        'arrivalID'=>-1
    );
    
    $query = "SELECT flightNumber, price, aircraftID, departureID, arrivalID 
     FROM Flight WHERE flightID = $flightID;";
    $db = dbConnect();
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain flightFields<br />$query<br />");
    while($row = mysqli_fetch_assoc($result))
    {
        $flightFields['flightNumber'] = $row['flightNumber'];
        $flightFields['price'] = $row['price'];
        $flightFields['aircraftID'] = $row['aircraftID'];
        $flightFields['departureID'] = $row['departureID'];
        $flightFields['arrivalID'] = $row['arrivalID'];
    }    
    mysqli_close($db);
    
    return $flightFields;
}

function getAircraftFields($aircraftID)
{
    $aircraftFields = array(
        'aircraftRegistration'=>'',
        'modelName'=>''
    );
    
    $modelID = -1;
    $query = "SELECT aircraftRegistration, modelID FROM Aircraft WHERE aircraftID = $aircraftID;";
    $db = dbConnect();
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain aircraftFields<br />$query<br />");
    while($row = mysqli_fetch_assoc($result))
    {
        $aircraftFields['aircraftRegistration'] = $row['aircraftRegistration'];
        $modelID = $row['modelID'];
    }

    $aircraftFields['modelName'] = getModelName($modelID);
    mysqli_close($db);
    
    return $aircraftFields;
}

function getModelName($modelID)
{
    $modelName = '';
    
    $query = "SELECT modelName FROM Model WHERE modelID = $modelID;";
    $db = dbConnect();
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain modelName<br />$query<br />");
    if($result) $modelName = mysqli_fetch_assoc($result)['modelName'];
    mysqli_close($db);
    
    return $modelName;
}

function getDepartureInfo($departureID)
{
    $departureInfo = array(
        'date'=>'',
        'time'=>'',
        'airport'=>array(
            'id'=>-1,
            'code'=>'',
            'name'=>'',
            'address'=>'',
            'city'=>'',
            'state'=>'',
            'zip'=>-1
        )
    );
    
    $airportID = -1;
    $query = "SELECT departureDate, departureTime, airportID
    FROM Departure WHERE departureID = $departureID;";
    $db = dbConnect();
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain departureInfo<br />$query<br />");
    while($row = mysqli_fetch_assoc($result))
    {
        $departureInfo['date'] = $row['departureDate'];
        $departureInfo['time'] = $row['departureTime'];
        $airportID = $row['airportID'];
    }
    mysqli_close($db);
    
    $airportInfo = getAirportInfo($airportID);
    $departureInfo['airport'] = $airportInfo;
    return $departureInfo;
}

function getArrivalInfo($arrivalID)
{
    $arrivalInfo = array(
        'date'=>'',
        'time'=>'',
        'airport'=>array(
            'id'=>-1,
            'code'=>'',
            'name'=>'',
            'address'=>'',
            'city'=>'',
            'state'=>'',
            'zip'=>-1
        )
    );

    $airportID = -1;
    $query = "SELECT arrivalDate, arrivalTime, airportID 
            FROM Arrival WHERE arrivalID = $arrivalID;";
    $db = dbConnect();
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain arrivalInfo<br />$query<br />");
    while($row = mysqli_fetch_assoc($result))
    {
        $arrivalInfo['date'] = $row['arrivalDate'];
        $arrivalInfo['time'] = $row['arrivalTime'];
        $airportID = $row['airportID'];
    }
    mysqli_close($db);
    
    $airportInfo = getAirportInfo($airportID);
    $arrivalInfo['airport'] = $airportInfo;
    return $arrivalInfo;
}

function getAirportInfo($airportID)
{
    $airportInfo = array(
        'id'=>-1,
        'code'=>'',
        'name'=>'',
        'address'=>'',
        'city'=>'',
        'state'=>'',
        'zip'=>-1
    );
    
    $cityID = -1;
    $stateID = -1;
    
    $query = "SELECT airportID, airportCode, airportName, address, cityID, stateID, ZIP 
            FROM Airport WHERE airportID = '$airportID';";
    $db = dbConnect();
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain airportInfo<br />$query<br />");
    while($row = mysqli_fetch_assoc($result))
    {
        $airportInfo['id'] = $row['airportID'];
        $airportInfo['code'] = $row['airportCode'];
        $airportInfo['name'] = $row['airportName'];
        $airportInfo['address'] = $row['address'];
        $airportInfo['zip'] = $row['ZIP'];
        
        $cityID = $row['cityID'];
        $stateID = $row['stateID'];
    }
    mysqli_close($db);
    
    $airportInfo['city'] = getCityName($cityID);
    $airportInfo['state'] = getStateName($stateID);
    return $airportInfo;
}

function getCityName($cityID)
{
    $cityName = '';
    $query = "SELECT cityName FROM City WHERE cityID = '$cityID';";
    $db = dbConnect();
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain cityName<br />$query<br />");
    if($result) $cityName = mysqli_fetch_assoc($result)['cityName'];    
    mysqli_close($db);
    return $cityName;
}

function getStateName($stateID)
{
    $stateName = '';
    $query = "SELECT stateName FROM State WHERE stateID = '$stateID';";
    $db = dbConnect();
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain stateName<br />$query<br />");
    if($result) $stateName = mysqli_fetch_assoc($result)['stateName'];
    mysqli_close($db);
    return $stateName;
}
?>