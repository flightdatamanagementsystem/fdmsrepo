<?php
/* Kristin Hamilton
 * created 25-Apr-2016
 */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

function getGuestCustomerInfo()
{
    $customerInfo = array(
        'username'=>'',
        'firstName' => '',
        'lastName' => '',
        'email' => '',
        'phone' => '',
        'DoB'=>array(
            'day'=>-1,
            'month'=>-1,
            'year'=>-1
        )
    );
    return $customerInfo;
}

function getCustomerInfoFromSessionArray()
{
    $savedCustomerInfo = array();
    $customerInfo = array(
        'username'=>'', 
        'firstName' => '',
        'lastName' => '',
        'email' => '',
        'phone' => '',
        'DoB'=>array(
            'day'=>-1,
            'month'=>-1,
            'year'=>-1
        )
    );
    
    if(isset($_SESSION['status']) && $_SESSION['status'] == 1)
    {
        if(isset($_SESSION['username']))
        {
            $username = htmlentities($_SESSION['username']);
            $customerInfo['username'] = $username;
            $savedCustomerInfo = getCustomerInfo($username);
        }
    
        // prepopulate customerInfo and passenger1 if possible:
        // default to $_SESSION values; otherwise (if not set), look up in db
        if(isset($_SESSION['firstName'])){ $customerInfo['firstName'] = $_SESSION['firstName']; }
        else { $customerInfo['firstName'] = $savedCustomerInfo['firstName']; }
    
        if(isset($_SESSION['lastName'])){ $customerInfo['lastName'] = $_SESSION['lastName']; }
        else { $customerInfo['lastName'] = $savedCustomerInfo['lastName']; }
    
        if(isset($_SESSION['email'])){ $customerInfo['email'] = $_SESSION['email']; }
        else { $customerInfo['email'] = $savedCustomerInfo['email']; }
    
        if(isset($_SESSION['phone'])){ $customerInfo['phone'] = $_SESSION['phone']; }
        else { $customerInfo['phone'] = $savedCustomerInfo['phone']; }
    
        if(isset($_SESSION['DoB']))
        {
            $customerInfo['DoB'] = splitUserDobIntoDMY($_SESSION['DoB']);
        }
        else
        {
            $customerInfo['DoB'] = splitUserDobIntoDMY($savedCustomerInfo['DoB']);
        }
    }
    else
    {
        $query = "INSERT INTO User VALUES(NULL, );";
    }
    
    return $customerInfo;
}

function getFlightInfoFromPostArray()
{
    $flightInfo = array(
        'travelerCount'=>1,
        'flightNumber'=>-1,
        'price'=>0.0,
        'aircraftRegistration'=>'',
        'modelName'=>'',
        'departureInfo'=>array(
            'date'=>'',
            'time'=>'',
            'airport'=>array(
                'id'=>-1,
                'code'=>'',
                'name'=>'',
                'address'=>'',
                'city'=>'',
                'state'=>'',
                'zip'=>-1
            )
        ),
        'arrivalInfo'=>array(
            'date'=>'',
            'time'=>'',
            'airport'=>array(
                'id'=>-1,
                'code'=>'',
                'name'=>'',
                'address'=>'',
                'city'=>'',
                'state'=>'',
                'zip'=>-1
            )
        )
    );
    

    if(isset($_POST['flightInfo']))
    {
        $unFlightInfo = unserialize($_POST['flightInfo']);
        $flightInfo['travelerCount'] = htmlentities($unFlightInfo['travelerCount']);
        $flightInfo['flightNumber'] = htmlentities($unFlightInfo['flightNumber']);
        $flightInfo['price'] = htmlentities($unFlightInfo['price']);
        $flightInfo['aircraftRegistration'] = htmlentities($unFlightInfo['aircraftRegistration']);
        $flightInfo['modelName'] = htmlentities($unFlightInfo['modelName']);
    
        $flightInfo['departureInfo']['date'] = htmlentities($unFlightInfo['departureInfo']['date']);
        $flightInfo['departureInfo']['time'] = htmlentities($unFlightInfo['departureInfo']['time']);
        $flightInfo['departureInfo']['airport']['id'] = htmlentities($unFlightInfo['departureInfo']['airport']['id']);
        $flightInfo['departureInfo']['airport']['code'] = htmlentities($unFlightInfo['departureInfo']['airport']['code']);
        $flightInfo['departureInfo']['airport']['name'] = htmlentities($unFlightInfo['departureInfo']['airport']['name']);
        $flightInfo['departureInfo']['airport']['address'] = htmlentities($unFlightInfo['departureInfo']['airport']['address']);
        $flightInfo['departureInfo']['airport']['city'] = htmlentities($unFlightInfo['departureInfo']['airport']['city']);
        $flightInfo['departureInfo']['airport']['state'] = htmlentities($unFlightInfo['departureInfo']['airport']['state']);
        $flightInfo['departureInfo']['airport']['zip'] = htmlentities($unFlightInfo['departureInfo']['airport']['zip']);
    
        $flightInfo['arrivalInfo']['date'] = htmlentities($unFlightInfo['arrivalInfo']['date']);
        $flightInfo['arrivalInfo']['time'] = htmlentities($unFlightInfo['arrivalInfo']['time']);
        $flightInfo['arrivalInfo']['airport']['id'] = htmlentities($unFlightInfo['arrivalInfo']['airport']['id']);
        $flightInfo['arrivalInfo']['airport']['code'] = htmlentities($unFlightInfo['arrivalInfo']['airport']['code']);
        $flightInfo['arrivalInfo']['airport']['name'] = htmlentities($unFlightInfo['arrivalInfo']['airport']['name']);
        $flightInfo['arrivalInfo']['airport']['address'] = htmlentities($unFlightInfo['arrivalInfo']['airport']['address']);
        $flightInfo['arrivalInfo']['airport']['city'] = htmlentities($unFlightInfo['arrivalInfo']['airport']['city']);
        $flightInfo['arrivalInfo']['airport']['state'] = htmlentities($unFlightInfo['arrivalInfo']['airport']['state']);
        $flightInfo['arrivalInfo']['airport']['zip'] = htmlentities($unFlightInfo['arrivalInfo']['airport']['zip']);
    }
    
    return $flightInfo;
}

//TODO
function getSeatSelectionFromPostArray()
{
    $seatSelection = array();
    return $seatSelection;
}

function getCustomerInfoFromPostArray()
{
    $customerInfo = array(
            'firstName' => '',
            'lastName' => '',
            'email' => '',
            'phone' => ''
    );
    
    if(isset($_POST['customerFirstName']))
    {
        $customerInfo['firstName'] = htmlentities($_POST['customerFirstName']);
    }
    if(isset($_POST['customerLastName']))
    {
        $customerInfo['lastName'] = htmlentities($_POST['customerLastName']);
    }
    if(isset($_POST['customerEmailAddress']))
    {
        $customerInfo['email'] = htmlentities($_POST['customerEmailAddress']);
    }
    if(isset($_POST['customerPhoneNumber']))
    {
        $customerInfo['phone'] = htmlentities($_POST['customerPhoneNumber']);
    }

    return $customerInfo;
}

function getPassengerInfoFromPostArray()
{   
    $passengerInfo = array(
        0=>array(
            'firstName' => '',
            'lastName' => '',
            'DoB' => array(
                'day' => -1,
                'month' => -1,
                'year' => -1
            )
        )
    );
    
    if(isset($_POST['passengerFirstName']))
    {
        $n = count($_POST['passengerFirstName']);
        
        for($i = 0; $i < $n; $i++)
        {
            if(isset($_POST['passengerFirstName']))
            {
                $passengerInfo[$i]['firstName'] = htmlentities($_POST['passengerFirstName'][$i]);
            }
            if(isset($_POST['passengerLastName']))
            {
                $passengerInfo[$i]['lastName'] = htmlentities($_POST['passengerLastName'][$i]);
            }    
            if(isset($_POST['passengerDobDay']))
            {
                $passengerInfo[$i]['DoB']['day'] = htmlentities($_POST['passengerDobDay'][$i]);
            }
            if(isset($_POST['passengerDobMonth']))
            {
                $passengerInfo[$i]['DoB']['month'] = htmlentities($_POST['passengerDobMonth'][$i]);
            }
            if(isset($_POST['passengerDobYear']))
            {
                $passengerInfo[$i]['DoB']['year'] = htmlentities($_POST['passengerDobYear'][$i]);
            }
        }
    }
    return $passengerInfo;
}

function getPaymentInfoFromPostArray()
{
    $paymentInfo = array(
            'paymentTypeName' => '',
            'cardNumber' => '',
            'cardholderName' => '',
            'cardExpiration' => array(
                    'month' => -1,
                    'year' => -1
            ) ,
            'cardSecurityCode'=>''
    );
    
    if(isset($_POST['submitPaymentInfo']))
    {
        $paymentInfo['paymentTypeName'] = htmlentities($_POST['submitPaymentInfo']);
    }
    
    if($paymentInfo['paymentTypeName'] != '')
    {
        $paymentType = $paymentInfo['paymentTypeName'];
        if(strnatcasecmp($paymentType, 'Credit') == 0)
        {
            if(isset($_POST['cardNumber_credit']))
            {
                $paymentInfo['cardNumber'] = htmlentities($_POST['cardNumber_credit']);
            }
            if(isset($_POST['cardholderName_credit']))
            {
                $paymentInfo['cardholderName'] = htmlentities($_POST['cardholderName_credit']);
            }
            if(isset($_POST['cardExpirationMonth_credit']))
            {
                $paymentInfo['cardExpiration']['month'] = htmlentities($_POST['cardExpirationMonth_credit']);
            }
            if(isset($_POST['cardExpirationYear_credit']))
            {
                $paymentInfo['cardExpiration']['year'] = htmlentities($_POST['cardExpirationYear_credit']);
            }
            if(isset($_POST['cardSecurityCode_credit']))
            {
                $paymentInfo['cardSecurityCode'] = htmlentities($_POST['cardSecurityCode_credit']);
            }
        }
        else
        {
            if(isset($_POST['cardNumber_debit']))
            {
                $paymentInfo['cardNumber'] = htmlentities($_POST['cardNumber_debit']);
            }
            if(isset($_POST['cardholderName_debit']))
            {
                $paymentInfo['cardholderName'] = htmlentities($_POST['cardholderName_debit']);
            }
            if(isset($_POST['cardExpirationMonth_debit']))
            {
                $paymentInfo['cardExpiration']['month'] = htmlentities($_POST['cardExpirationMonth_debit']);
            }
            if(isset($_POST['cardExpirationYear_debit']))
            {
                $paymentInfo['cardExpiration']['year'] = htmlentities($_POST['cardExpirationYear_debit']);
            }
            if(isset($_POST['cardSecurityCode_debit']))
            {
                $paymentInfo['cardSecurityCode'] = htmlentities($_POST['cardSecurityCode_debit']);
            }
        }
    }
    
    return $paymentInfo;
}
?>