<?php 
//session_start();
/* Kristin Hamilton
 * created 29-Mar-2016
 */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

spl_autoload_register(function($className){
    include $className . ".php";
});

interface Booking
{
    /* mutator methods */
    
    function setCustomer($customer);
    
    // sets seat selection for this booking
    // returns nothing
    function setSeatSelection($seatSelection);
    
    // sets passenger info for this booking
    // returns nothing
    function setPassengerList($passengerList);
     
    // sets payment info for this booking
    // returns nothing
    function setBookingPayment($bookingPayment);
    
    /* accessor methods */
    
    function getCustomer();
    
    // returns seat selection for this booking
    function getSeatSelection();
    
    // returns passenger list for this booking
    function getPassengerList();
     
    // returns payment info for this booking
    function getBookingPayment();    
    
    function getBasePriceTotal();
    function getTaxesAndFeesTotal();
    function getFinalPriceTotal();
    
    function getFlight1();
    function getFlight2();
    
    function processPayment($bookingPayment);
    function getConfirmationNumber();
}
?>