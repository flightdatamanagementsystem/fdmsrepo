<?php
/* Kristin Hamilton */
function insertGuestCustomerIntoDb()
{
    $query = "INSERT INTO User VALUES(Null, 'guest', 'guest', 'guest', 'guest', '')";
}

function updateDbs($booking)
{
    $customer = $booking->getCustomer();
    $passengerList = $booking->getPassengerList();
    $bookingPayment = $booking->getBookingPayment();
    
    $userID = -1;    
    $flightID_1 = -1;
    $flightID_2 = -1;
    $paymentDate = date('Y-m-d');
    
    $confNumber = $booking->getConfirmationNumber();
    $flight1 = $booking->getFlight1();
    $flight2 = $booking->getFlight2();
    $flightNumber_1 = $flight1->getFlightNumber();
    $flightNumber_2 = $flight2->getFlightNumber();
    $ticketCount = $flight1->getFlightTravelerCount();
    
    $paymentAmount = $booking->getFinalPriceTotal();
    $paymentCardNumber = $bookingPayment->getBookingPaymentCardNumber();
    $paymentCardholderName = $bookingPayment->getBookingPaymentCardholderName();
    $paymentCardExpMonth = $bookingPayment->getBookingPaymentCardExpirationMonth();
    $paymentCardExpYear = $bookingPayment->getBookingPaymentCardExpirationYear();
    $paymentCardSecurityCode = $bookingPayment->getBookingPaymentCardSecurityCode();
    $paymentTypeName = $bookingPayment->getBookingPaymentTypeName();
    $paymentTypeID = getPaymentTypeID($paymentTypeName);
    
    $firstName = $customer->getCustomerFirstName();
    $lastName = $customer->getCustomerLastName();
    $username = $customer->getCustomerUsername();
    $userID = getUserIdNumber($username);
    $flightID_1 = $flight1->getFlightID();
    $flightID_2 = $flight2->getFlightID();
    
    $db = dbConnect();
    $query = "INSERT INTO FlightUser VALUES($flightID_1, $userID)";
    $result = mysqli_query($db, $query);  //or die("<br />Error: unable to insert into flightUser1<br />$query<br />");
    $query = "INSERT INTO FlightUser VALUES($flightID_2, $userID);";
    $result = mysqli_query($db, $query);  //or die("<br />Error: unable to insert into flightUser2<br />$query<br />");
    
    
    $query = "INSERT INTO Payment VALUES(Null, '$paymentDate', $paymentAmount, $flightID_1, $userID)";
    $result = mysqli_query($db, $query) or die("<br />Error: unable to insert into payment1<br />$query<br />");
    $query = "INSERT INTO Payment VALUES(Null, '$paymentDate', $paymentAmount, $flightID_2, $userID);";
    $result = mysqli_query($db, $query) or die("<br />Error: unable to insert into payment2<br />$query<br />");
    
    $query = "INSERT INTO PaymentMethod VALUES 
                (Null, $paymentTypeID, $paymentCardNumber, '$paymentCardholderName', 
                 $paymentCardExpYear, $paymentCardExpMonth, $paymentCardSecurityCode, $userID);";
    $result = mysqli_query($db, $query) or die("<br />Error: unable to insert into paymentMethod<br />$query<br />");
    
    $query = "INSERT INTO confNum VALUES(Null, '$confNumber');";
    $result = mysqli_query($db, $query);  //or die("<br />Error: unable to insert into confNum<br />$query<br />");
    
    $confNumID = -1;
    $query = "SELECT confNumID FROM ConfNum ORDER BY confNumID DESC LIMIT 1;";
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain confNumID<br />$query<br />"); 
    while($row = mysqli_fetch_assoc($result)) $confNumID = $row['confNumID'];
    
    $paymentID = -1;
    $query = "SELECT paymentID FROM Payment ORDER BY paymentID DESC LIMIT 1;";
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain paymentID<br />$query<br />");
    while($row = mysqli_fetch_assoc($result)) $paymentID = $row['paymentID'];
    
    $paymentMethodID = -1;
    $query = "SELECT paymentMethodID FROM PaymentMethod ORDER BY paymentMethodID DESC LIMIT 1;";
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain paymentMethodID<br />$query<br />");
    while($row = mysqli_fetch_assoc($result)) $paymentMethodID = $row['paymentMethodID'];
    
    $query = "INSERT INTO booking VALUES (Null, $userID, $confNumID, $flightID_1, $ticketCount, $paymentID, $paymentMethodID)";
    $result = mysqli_query($db, $query) or die("<br />Error: unable to insert into booking1<br />$query<br />");
    
    $query = "INSERT INTO booking VALUES (Null, $userID, $confNumID, $flightID_2, $ticketCount, $paymentID, $paymentMethodID)";
    $result = mysqli_query($db, $query) or die("<br />Error: unable to insert into booking2<br />$query<br />");
    
    $bookingID_flight1 = -1;
    $bookingID_flight2 = -1;
    $seatID_flight1 = "Null";
    $seatID_flight2 = "Null";
    $firstName = "";
    $lastName = "";
    $dateOfBirth = "";
    
    $query = "SELECT bookingID FROM Booking ORDER BY bookingID DESC LIMIT 2;";
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain bookingIDs<br />$query<br />");
    $bookingID_flight2 = mysqli_fetch_assoc($result)['bookingID']; echo "bookingID2 = $bookingID_flight2";
    $bookingID_flight1 = mysqli_fetch_assoc($result)['bookingID']; echo "bookingID1 = $bookingID_flight1";
    
    for($i = 0; $i < count($passengerList); $i++)
    {
        $firstName = $passengerList[$i]['firstName'];
        $lastName = $passengerList[$i]['lastName'];
        $dateOfBirth = $passengerList[$i]['DoB']['year']."-".
                $passengerList[$i]['DoB']['month']."-".
                $passengerList[$i]['DoB']['day'];
        
        $query = "INSERT INTO PassengerSeatBooking VALUES 
            (Null, $bookingID_flight1, $bookingID_flight2, $seatID_flight1, $seatID_flight2, 
            '$firstName', '$lastName', '$dateOfBirth');";
        $result = mysqli_query($db, $query) or 
            die("<br />Error: unable to insert into PassengerSeatBooking<br />$query<br />");
    }
    
    mysqli_close($db);
}

function getPaymentTypeID($paymentTypeName)
{
    $paymentTypeID = -1;
    $query = "SELECT paymentTypeID FROM PaymentType WHERE paymentTypeName = '$paymentTypeName';";
    $db = dbConnect();
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain paymentTypeID<br />$query<br />");
    if($result) $paymentTypeID = mysqli_fetch_row($result)[0];
    mysqli_close($db);
    return $paymentTypeID;
}

function getCustomerUsername($firstName, $lastName)
{
    $username = "";
    $query = "SELECT username FROM User WHERE firstName = '$firstName' and lastName = '$lastName';";
    $db = dbConnect();
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain username<br />$query<br />");
    if($result) $username = mysqli_fetch_row($result)[0];
    mysqli_close($db);
    return $username;
}
?>