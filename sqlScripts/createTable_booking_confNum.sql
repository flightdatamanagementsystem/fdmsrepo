/* Kristin Hamilton
 * created 04-Apr-2016
 * Desc: create table script for ConfNum, Booking, PassengerSeatBooking tables
 */

USE FlightDB;

DROP TABLE IF EXISTS PassengerSeatBooking;
DROP TABLE IF EXISTS Booking;
DROP TABLE IF EXISTS ConfNum;

/* create ConfNum table to hold confirmation numbers */

CREATE TABLE IF NOT EXISTS ConfNum (
    confNumID INT UNSIGNED AUTO_INCREMENT,    
    confNum CHAR(16) NOT NULL UNIQUE,    
    PRIMARY KEY(confNumId)    
) engine=innodb;

/* create Booking table */

CREATE TABLE IF NOT EXISTS Booking (
    bookingID INT UNSIGNED AUTO_INCREMENT,    
    userID INT NOT NULL,    
    confNumID INT UNSIGNED NOT NULL,    
    flightID INT(11) NOT NULL, 
    ticketCount INT UNSIGNED NOT NULL,  
    paymentID INT(11) NOT NULL, 
    paymentMethodID INT(11) NOT NULL,   
    PRIMARY KEY(bookingID),
    FOREIGN KEY(confNumID) REFERENCES ConfNum(confNumID),
    FOREIGN KEY(flightID) REFERENCES Flight(flightID),  
    FOREIGN KEY(paymentID) REFERENCES Payment(paymentID), 
    FOREIGN KEY(paymentMethodID) REFERENCES PaymentMethod(paymentMethodID)     
) engine=innodb;

/* create PassengerSeatBooking table */

CREATE TABLE IF NOT EXISTS PassengerSeatBooking (
    passengerSeatBookingID INT UNSIGNED AUTO_INCREMENT,
    bookingID_flight1 INT UNSIGNED NOT NULL,
    bookingID_flight2 INT UNSIGNED NOT NULL,         
    seatID_flight1 INT(11),
    seatID_flight2 INT(11),    
    firstName VARCHAR(30) NOT NULL,    
    lastName VARCHAR(30) NOT NULL,    
    dateOfBirth DATE NOT NULL,    
    PRIMARY KEY(passengerSeatBookingID),    
    FOREIGN KEY(bookingID_flight1) REFERENCES Booking(bookingID),    
    FOREIGN KEY(bookingID_flight2) REFERENCES Booking(bookingID),  
    FOREIGN KEY(seatID_flight1) REFERENCES Seat(seatID), 
    FOREIGN KEY(seatID_flight2) REFERENCES Seat(seatID)      
) engine=innodb;
