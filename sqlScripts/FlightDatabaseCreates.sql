USE FlightDB;

DROP TABLE IF EXISTS PassengerSeatBooking;
DROP TABLE IF EXISTS Booking;
DROP TABLE IF EXISTS ConfNum;
DROP TABLE IF EXISTS PaymentMethod;
DROP TABLE IF EXISTS PaymentType;
DROP TABLE IF EXISTS Payment;
DROP TABLE IF EXISTS FlightUser;
DROP TABLE IF EXISTS Flight;
DROP TABLE IF EXISTS Arrival;
DROP TABLE IF EXISTS Departure;
DROP TABLE IF EXISTS Airport;
DROP TABLE IF EXISTS State;
DROP TABLE IF EXISTS City;
DROP TABLE IF EXISTS Seat;
DROP TABLE IF EXISTS Aircraft;
DROP TABLE IF EXISTS Model;
DROP TABLE IF EXISTS User;

create table User(
    userID int auto_increment not null, 
    username varchar(30) not null, 
    password varchar(30) not null, 
    firstName varchar(30) not null, 
    lastName varchar(30) not null, 
    DoB date not null, 
    email varchar(254), 
    phone numeric(10, 0), 
    status numeric(1, 0) not null, 
    primary key(userID)) 
    engine=innodb;

create table Model(
    modelID int auto_increment not null, 
    modelName varchar(50) not null, 
    primary key(modelID)) 
    engine=innodb;

create table Aircraft(
    aircraftID int auto_increment not null, 
    aircraftRegistration varchar(6) not null, 
    modelID int not null, 
    primary key(aircraftID), 
    foreign key(modelID) references Model(modelID)) 
    engine=innodb;

create table Seat(
    seatID int auto_increment not null, 
    seatName varchar(10) not null, 
    aircraftID int not null, 
    userID int, 
    primary key(seatID), 
    foreign key(aircraftID) references Aircraft(aircraftID) on delete cascade, 
    foreign key(userID) references User(userID) on delete set null) 
    engine=innodb;

create table City(
    cityID int auto_increment not null, 
    cityName varchar(25) not null, 
    primary key(cityID)) 
    engine=innodb;

create table State(
    stateID int auto_increment not null, 
    stateName varchar(2) not null, 
    primary key(stateID)) 
    engine=innodb;

create table Airport(
    airportID int auto_increment not null, 
    airportCode varchar(4) not null, 
    airportName varchar(50) not null, 
    address varchar(100) not null, 
    cityID int not null, 
    stateID int not null, 
    ZIP numeric(5) not null, 
    primary key(airportID), 
    foreign key(cityID) references City(cityID), 
    foreign key(stateID) references State(stateID)) 
    engine=innodb;

create table Departure(
    departureID int auto_increment not null, 
    departureDate date not null, 
    departureTime time not null, 
    airportID int not null, 
    cityID int not null, 
    primary key(departureID), 
    foreign key(airportID) references Airport(airportID), 
    foreign key(cityID) references City(cityID)) 
    engine=innodb;

create table Arrival(
    arrivalID int auto_increment not null, 
    arrivalDate date not null, 
    arrivalTime time not null, 
    airportID int not null, 
    cityID int not null, 
    primary key(arrivalID), 
    foreign key(airportID) references Airport(airportID), 
    foreign key(cityID) references City(cityID)) 
    engine=innodb;

create table Flight(
    flightID int auto_increment not null, 
    flightNumber varchar(4) not null, 
    price numeric(7, 2), 
    aircraftID int, 
    departureID int, 
    arrivalID int, 
    primary key(flightID), 
    foreign key(aircraftID) references Aircraft(aircraftID) on delete set null, 
    foreign key(departureID) references Departure(departureID) on delete set null, 
    foreign key(arrivalID) references Arrival(arrivalID) on delete set null)
    engine=innodb;

create table FlightUser(
    flightID int not null, 
    userID int not null, 
    primary key(flightID, userID), 
    foreign key(flightID) references Flight(flightID) on delete cascade, 
    foreign key(userID) references User(userID) on delete cascade) 
    engine=innodb;

create table Payment(
    paymentID int auto_increment not null, 
    paymentDate date not null, 
    paymentAmount numeric(7, 2) not null, 
    flightID int not null, 
    userID int not null, 
    primary key(paymentID), 
    foreign key(flightID) references Flight(flightID) on delete cascade, 
    foreign key(userID) references User(userID) on delete cascade) 
    engine=innodb;

create table PaymentType(
    paymentTypeID int auto_increment not null, 
    paymentTypeName varchar(10) not null, 
    primary key(paymentTypeID))
    engine=innodb;

create table PaymentMethod(
    paymentMethodID int auto_increment not null, 
    paymentTypeID int not null, 
    cardNumber numeric(16, 0) not null, 
    cardHolderName varchar(50) not null, 
    expirationYear numeric(4, 0) not null, 
    expirationMonth numeric(2, 0) not null, 
    securityCode numeric(4, 0) not null, 
    userID int not null, 
    primary key(paymentMethodID), 
    foreign key(paymentTypeID) references PaymentType(paymentTypeID) on delete cascade, 
    foreign key(userID) references User(userID) on delete cascade) 
    engine=innodb;
    
/* Kristin Hamilton
 * created 04-Apr-2016
 * Desc: create table script for ConfNum, Booking, PassengerSeatBooking tables
 */

USE FlightDB;

DROP TABLE IF EXISTS PassengerSeatBooking;
DROP TABLE IF EXISTS Booking;
DROP TABLE IF EXISTS ConfNum;

/* create ConfNum table to hold confirmation numbers */

CREATE TABLE IF NOT EXISTS ConfNum (
    confNumID INT UNSIGNED AUTO_INCREMENT,    
    confNum CHAR(16) NOT NULL UNIQUE,    
    PRIMARY KEY(confNumId)    
) engine=innodb;

/* create Booking table */

CREATE TABLE IF NOT EXISTS Booking (
    bookingID INT UNSIGNED AUTO_INCREMENT,    
    userID INT NOT NULL,    
    confNumID INT UNSIGNED NOT NULL,    
    flightID INT(11) NOT NULL, 
    ticketCount INT UNSIGNED NOT NULL,  
    paymentID INT(11) NOT NULL, 
    paymentMethodID INT(11) NOT NULL,   
    PRIMARY KEY(bookingID),
    FOREIGN KEY(confNumID) REFERENCES ConfNum(confNumID),
    FOREIGN KEY(flightID) REFERENCES Flight(flightID),  
    FOREIGN KEY(paymentID) REFERENCES Payment(paymentID), 
    FOREIGN KEY(paymentMethodID) REFERENCES PaymentMethod(paymentMethodID)     
) engine=innodb;

/* create PassengerSeatBooking table */

CREATE TABLE IF NOT EXISTS PassengerSeatBooking (
    passengerSeatBookingID INT UNSIGNED AUTO_INCREMENT,
    bookingID_flight1 INT UNSIGNED NOT NULL,
    bookingID_flight2 INT UNSIGNED NOT NULL,         
    seatID_flight1 INT(11) NOT NULL,
    seatID_flight2 INT(11) NOT NULL,    
    firstName VARCHAR(30) NOT NULL,    
    lastName VARCHAR(30) NOT NULL,    
    dateOfBirth DATE NOT NULL,    
    PRIMARY KEY(passengerSeatBookingID),    
    FOREIGN KEY(bookingID_flight1) REFERENCES Booking(bookingID),    
    FOREIGN KEY(bookingID_flight2) REFERENCES Booking(bookingID),  
    FOREIGN KEY(seatID_flight1) REFERENCES Seat(seatID), 
    FOREIGN KEY(seatID_flight2) REFERENCES Seat(seatID)      
) engine=innodb;
    