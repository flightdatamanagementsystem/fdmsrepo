<?php
/* BookingTest.php
 */

class BookingTest extends PHPUnit_Framework_Testcase
{
    public function travelerListIsEmpty()
    {
        $flight = array();
        $booking = new BookingImpl($flight);
        $travelerList = array();
        
        $this->expectOutputString("Error: Passenger list must contain at least one traveler");
        $booking->setTravelerList($travelerList);        
    }
}

?>