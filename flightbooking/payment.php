<?php
echo "<!DOCTYPE html>".
		"<html lang='en'>".
		"<head>".
		"<meta charset='utf-8'>".
		"<meta name='viewport' content='width=device-width, initial scale=1.0'>".
		"<meta name='author' content='Nikko'>".
		"<link rel='icon' href='./favicon.ico' type='image/x-icon'>".
		"<link rel='shortcut icon' href='./favicon.ico' type='image/x-icon'>".
		"<link rel='stylesheet' href='./flight.css' type='text/css'>".
		"<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway' type='text/css'>".
		"<title>Flight</title>".
		"</head>";

require "connect.php";
require "flightDatabaseFunctions.php";

session_start();
$connection = connect();
$_SESSION["userID"] = getUserID($connection, $_SESSION["username"]);
$connection->close();

echo "<body>";
echo "<header class='header'></header>";
echo "<div class='content'>";

if($_POST['history']){
	viewPaymentHistory();
}
elseif($_POST['add']){
	addPaymentMethodForm();
}
elseif($_POST['addPaymentMethod']){
	checkPaymentMethod();
}
elseif($_POST['remove']){
	removePaymentMethodForm();
}
elseif($_POST['removePaymentMethod']){
	removePaymentMethod();
}
else{
	viewPaymentInfo();
}

echo "</div>";
echo "<div class='footerContainer'>";
echo "<div class='footer'>Copyright &copy 2016 Awesome Airlines. All rights reserved.</div>";
echo "</div>";
echo "</body>";

// Displays the payment page
function viewPaymentInfo(){
	echo "<h4>Payment Information</h4>".
			"<form method='post'>".
			"<input type='submit' class='button' name='history' value='View Payment History'>".
			"<input type='submit' class='button' name='add' value='Add Payment Method'>".
			"<input type='submit' class='button' name='remove' value='Remove Payment Method'>".
			"</form>".
			"<h4>Payment Methods</h4>";
	$connection = connect();
	$result = getUserPaymentMethods($connection, $_SESSION["userID"]);
	if($result->num_rows > 0){
		echo "<table class='invisibleTable'>";
		while($row = $result->fetch_assoc()){
			$paymentMethodID = $row['paymentMethodID'];
			$paymentMethod = getPaymentMethod($connection, $paymentMethodID);
			echo "<tr>".
					"<td>".$paymentMethod['paymentTypeName']."</td>".
					"<td>XXXX-XXXX-XXXX-".$paymentMethod['cardNumber']."</td>".
					"</tr>";
		}
		echo "</table>";
	}
	else{
		echo "(None)";
	}
	$connection->close();
}

// Displays the payment history page
function viewPaymentHistory(){
	echo "<h4>Payment History</h4>".
			"<table class='table'>".
			"<tr>".
			"<th>Payment Date</th>".
			"<th>Payment Amount</th>".
			"<tr>";
	$connection = connect();
	$result = getUserPayments($connection, $_SESSION["userID"]);
	while($row = $result->fetch_assoc()){
		$paymentID = $row['paymentID'];
		$payment = getPayment($connection, $paymentID);
		echo "<tr>".
				"<td>".formatDate($payment['paymentDate'])."</td>".
				"<td>$".$payment['paymentAmount']."</td>".
				"</tr>";
	}
	$connection->close();
	echo "</table>";
}

function addPaymentMethodForm($cardNumber = ""){
	echo "<h4>Add Credit or Debit Card</h4>".
			"<font color='red'>Payment processing will be handled by an external service.</font><br><br>".
			"<form method='post'>".
			"Card type: <select name='paymentType'>";
	$connection = connect();
	$result = getPaymentType($connection);
	while($row = $result->fetch_assoc()){
		$paymentTypeID = $row['paymentTypeID'];
		$paymentType = $row['paymentTypeName'];
		echo "<option value='".$paymentTypeID."'>".$paymentType."</option>";
	}
	$connection->close();
	echo "</select><br>".
			"Last four digits of card number: <input type='text' name='cardNumber'><br><br>".
			"<input type='submit' class='button' name='addPaymentMethod' value='Add'>".
			"<input type='submit' class='button' name='cancel' value='Cancel'>".
			"</form>";
}

function checkPaymentMethod(){
	$paymentType = $_POST['paymentType'];
	$cardNumber = $_POST['cardNumber'];
	if(!preg_match_all("/\d{4}/", $cardNumber, $matches)){
		echo "<font color='red'>Please enter a valid number.</font>";
		addPaymentMethodForm($cardNumber);
	}
	else{
		$connection = connect();
		insertPaymentMethod($connection, $paymentType, $cardNumber, $_SESSION["userID"]);
		$connection->close();
		viewPaymentInfo();
	}
}

function removePaymentMethodForm(){
	echo "<h4>Payment Methods</h4>".
			"<form method='post'>";
	$connection = connect();
	$result = getUserPaymentMethods($connection, $_SESSION["userID"]);
	if($result->num_rows > 0){
		while($row = $result->fetch_assoc()){
			$paymentMethodID = $row['paymentMethodID'];
			$paymentMethod = getPaymentMethod($connection, $paymentMethodID);
			$paymentType = $paymentMethod['paymentTypeName'];
			$cardNumber = $paymentMethod['cardNumber'];
			echo "<input type='radio' name='paymentMethodID' value='".$paymentMethodID."'>".$paymentType." ".$cardNumber."<br>";
		}
	}
	echo "<br><input type='submit' class='button' name='removePaymentMethod' value='Remove'>".
			"<input type='submit' class='button' name='cancel' value='Cancel'>".
			"</form>";
}

function removePaymentMethod(){
	$connection = connect();
	$paymentMethodID = $_POST['paymentMethodID'];
	deletePaymentMethod($connection, $paymentMethodID);
	$connection->close();
	viewPaymentInfo();
}
?>