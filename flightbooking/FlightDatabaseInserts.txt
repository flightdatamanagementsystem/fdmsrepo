use FlightDB;
insert into User values(Null, 'FlightAdministrator', 'COSC4157', 'Jane', 'Doe', '1900-01-01', 'JaneDoe@mail.com', 

5551234567, 0);
insert into User values(Null, 'Snoop_Dizzle', 'FoShizzle', 'Snoop', 'Dogg', '1971-10-20', 'SnoopDogg@ogmail.com', Null, 1);
insert into User values(Null, 'Lil_Jon', 'TurnDown4What', 'Jonathan', 'Smith', '1971-01-17', 'LilJon@ogmail.com', Null, 1);
insert into User values(Null, 'Moonwalker', 'SmoothCriminal', 'Michael', 'Jackson', '1958-08-29', 

'MichaelJackson@mail.com', Null, 1);
insert into User values(Null, 'HollabackGirl', 'BANANAS', 'Gwen', 'Stefani', '1969-10-03', 'GwenStefani@mail.com', Null, 

1);
insert into User values(Null, 'PANTERA', '5MinutesAlone', 'Phil', 'Anselmo', '1968-06-30', 'PhilAnselmo@mail.com', Null, 

1);
insert into User values(Null, 'Sk8erGrl', 'CUL8erBoi', 'Avril', 'Lavigne', '1984-09-27', 'AvrilLavigne@mail.com', Null, 1);
insert into User values(Null, 'xXx_SKR1LL3X_xXx', 'EQU1N0X', 'Sonny', 'Moore', '1988-01-15', 'Skrillex@mail.com', Null, 1);
insert into User values(Null, 'Jack_Sparrow', 'WhyIsTheRumGone', 'Johnny', 'Depp', '1963-06-09', 'JackSparrow@pirate.net', 

Null, 1);
insert into User values(Null, 'Will_Turner', 'ThatsNotGoodEnough', 'Orlando', 'Bloom', '1977-01-13', 

'WillTurner@pirate.net', Null, 1);
insert into User values(Null, 'TheTerminator', 'HastaLaVistaBaby', 'Arnold', 'Schwarzenegger', '1947-07-30', 

'TheTerminator@sky.net', Null, 1);
insert into User values(Null, 'LeonTheProfessional', 'EVERY1', 'Jean', 'Reno', '1948-07-30', 'Leon@cleaner.com', Null, 1);
insert into User values(Null, 'John_Wick', 'BabaYaga', 'Keanu', 'Reeves', '1964-09-02', 'JohnWick@cleaner.com', Null, 1);

insert into Model values(Null, 'Boeing 737');
insert into Model values(Null, 'Boeing 747');
insert into Model values(Null, 'Boeing 757');
insert into Model values(Null, 'Boeing 767');
insert into Model values(Null, 'Boeing 777');
insert into Model values(Null, 'Boeing 787');

insert into Aircraft values(Null, 'N12345', 1);
insert into Aircraft values(Null, 'N12346', 2);
insert into Aircraft values(Null, 'N12347', 3);
insert into Aircraft values(Null, 'N12348', 4);
insert into Aircraft values(Null, 'N12349', 5);
insert into Aircraft values(Null, 'N12350', 6);

insert into Seat values(Null, '1A', 1, Null);
insert into Seat values(Null, '2A', 1, Null);
insert into Seat values(Null, '3A', 1, Null);
insert into Seat values(Null, '4A', 1, Null);
insert into Seat values(Null, '5A', 1, Null);
insert into Seat values(Null, '6A', 1, Null);
insert into Seat values(Null, '7A', 1, Null);
insert into Seat values(Null, '8A', 1, Null);
insert into Seat values(Null, '9A', 1, Null);
insert into Seat values(Null, '10A', 1, Null);
insert into Seat values(Null, '11A', 1, Null);
insert into Seat values(Null, '12A', 1, Null);
insert into Seat values(Null, '13A', 1, Null);
insert into Seat values(Null, '14A', 1, Null);
insert into Seat values(Null, '15A', 1, Null);
insert into Seat values(Null, '16A', 1, Null);
insert into Seat values(Null, '17A', 1, Null);
insert into Seat values(Null, '18A', 1, Null);
insert into Seat values(Null, '19A', 1, Null);
insert into Seat values(Null, '20A', 1, Null);
insert into Seat values(Null, '21A', 1, Null);
insert into Seat values(Null, '22A', 1, Null);
insert into Seat values(Null, '23A', 1, Null);
insert into Seat values(Null, '1B', 1, Null);
insert into Seat values(Null, '2B', 1, Null);
insert into Seat values(Null, '3B', 1, Null);
insert into Seat values(Null, '4B', 1, Null);
insert into Seat values(Null, '5B', 1, Null);
insert into Seat values(Null, '6B', 1, Null);
insert into Seat values(Null, '7B', 1, Null);
insert into Seat values(Null, '8B', 1, Null);
insert into Seat values(Null, '9B', 1, Null);
insert into Seat values(Null, '10B', 1, Null);
insert into Seat values(Null, '11B', 1, Null);
insert into Seat values(Null, '12B', 1, Null);
insert into Seat values(Null, '13B', 1, Null);
insert into Seat values(Null, '14B', 1, Null);
insert into Seat values(Null, '15B', 1, Null);
insert into Seat values(Null, '16B', 1, Null);
insert into Seat values(Null, '17B', 1, Null);
insert into Seat values(Null, '18B', 1, Null);
insert into Seat values(Null, '19B', 1, Null);
insert into Seat values(Null, '20B', 1, Null);
insert into Seat values(Null, '21B', 1, Null);
insert into Seat values(Null, '22B', 1, Null);
insert into Seat values(Null, '23B', 1, Null);
insert into Seat values(Null, '1C', 1, Null);
insert into Seat values(Null, '2C', 1, Null);
insert into Seat values(Null, '3C', 1, Null);
insert into Seat values(Null, '4C', 1, Null);
insert into Seat values(Null, '5C', 1, Null);
insert into Seat values(Null, '6C', 1, Null);
insert into Seat values(Null, '7C', 1, Null);
insert into Seat values(Null, '8C', 1, Null);
insert into Seat values(Null, '9C', 1, Null);
insert into Seat values(Null, '10C', 1, Null);
insert into Seat values(Null, '11C', 1, Null);
insert into Seat values(Null, '12C', 1, Null);
insert into Seat values(Null, '13C', 1, Null);
insert into Seat values(Null, '14C', 1, Null);
insert into Seat values(Null, '15C', 1, Null);
insert into Seat values(Null, '16C', 1, Null);
insert into Seat values(Null, '17C', 1, Null);
insert into Seat values(Null, '18C', 1, Null);
insert into Seat values(Null, '19C', 1, Null);
insert into Seat values(Null, '20C', 1, Null);
insert into Seat values(Null, '21C', 1, Null);
insert into Seat values(Null, '22C', 1, Null);
insert into Seat values(Null, '23C', 1, Null);
insert into Seat values(Null, '1D', 1, Null);
insert into Seat values(Null, '2D', 1, Null);
insert into Seat values(Null, '3D', 1, Null);
insert into Seat values(Null, '4D', 1, Null);
insert into Seat values(Null, '5D', 1, Null);
insert into Seat values(Null, '6D', 1, Null);
insert into Seat values(Null, '7D', 1, Null);
insert into Seat values(Null, '8D', 1, Null);
insert into Seat values(Null, '9D', 1, Null);
insert into Seat values(Null, '10D', 1, Null);
insert into Seat values(Null, '11D', 1, Null);
insert into Seat values(Null, '12D', 1, Null);
insert into Seat values(Null, '13D', 1, Null);
insert into Seat values(Null, '14D', 1, Null);
insert into Seat values(Null, '15D', 1, Null);
insert into Seat values(Null, '16D', 1, Null);
insert into Seat values(Null, '17D', 1, Null);
insert into Seat values(Null, '18D', 1, Null);
insert into Seat values(Null, '19D', 1, Null);
insert into Seat values(Null, '20D', 1, Null);
insert into Seat values(Null, '21D', 1, Null);
insert into Seat values(Null, '22D', 1, Null);
insert into Seat values(Null, '23D', 1, Null);
insert into Seat values(Null, '1E', 1, Null);
insert into Seat values(Null, '2E', 1, Null);
insert into Seat values(Null, '3E', 1, Null);
insert into Seat values(Null, '4E', 1, Null);
insert into Seat values(Null, '5E', 1, Null);
insert into Seat values(Null, '6E', 1, Null);
insert into Seat values(Null, '7E', 1, Null);
insert into Seat values(Null, '8E', 1, Null);
insert into Seat values(Null, '9E', 1, Null);
insert into Seat values(Null, '10E', 1, Null);
insert into Seat values(Null, '11E', 1, Null);
insert into Seat values(Null, '12E', 1, Null);
insert into Seat values(Null, '13E', 1, Null);
insert into Seat values(Null, '14E', 1, Null);
insert into Seat values(Null, '15E', 1, Null);
insert into Seat values(Null, '16E', 1, Null);
insert into Seat values(Null, '17E', 1, Null);
insert into Seat values(Null, '18E', 1, Null);
insert into Seat values(Null, '19E', 1, Null);
insert into Seat values(Null, '20E', 1, Null);
insert into Seat values(Null, '21E', 1, Null);
insert into Seat values(Null, '22E', 1, Null);
insert into Seat values(Null, '23E', 1, Null);
insert into Seat values(Null, '1F', 1, Null);
insert into Seat values(Null, '2F', 1, Null);
insert into Seat values(Null, '3F', 1, Null);
insert into Seat values(Null, '4F', 1, Null);
insert into Seat values(Null, '5F', 1, Null);
insert into Seat values(Null, '6F', 1, Null);
insert into Seat values(Null, '7F', 1, Null);
insert into Seat values(Null, '8F', 1, Null);
insert into Seat values(Null, '9F', 1, Null);
insert into Seat values(Null, '10F', 1, Null);
insert into Seat values(Null, '11F', 1, Null);
insert into Seat values(Null, '12F', 1, Null);
insert into Seat values(Null, '13F', 1, Null);
insert into Seat values(Null, '14F', 1, Null);
insert into Seat values(Null, '15F', 1, Null);
insert into Seat values(Null, '16F', 1, Null);
insert into Seat values(Null, '17F', 1, Null);
insert into Seat values(Null, '18F', 1, Null);
insert into Seat values(Null, '19F', 1, Null);
insert into Seat values(Null, '20F', 1, Null);
insert into Seat values(Null, '21F', 1, Null);
insert into Seat values(Null, '22F', 1, Null);
insert into Seat values(Null, '23F', 1, Null);

insert into City values(Null, 'Austin');
insert into City values(Null, 'New York');
insert into City values(Null, 'Los Angeles');
insert into City values(Null, 'Las Vegas');

insert into State values(Null, 'TX');
insert into State values(Null, 'NY');
insert into State values(Null, 'CA');
insert into State values(Null, 'NV');

insert into Airport values(Null, 'KAUS', 'Austin-Bergstrom International Airport', '3600 Presidential Blvd.', 1, 1, 78719);
insert into Airport values(Null, 'KJFK', 'John F. Kennedy International Airport', 'Jamaica', 2, 2, 11430);
insert into Airport values(Null, 'KLAX', 'Los Angeles International Airport', '1 World Way', 3, 3, 90045);
insert into Airport values(Null, 'KLAS', 'McCarran International Airport', '5757 Wayne Newton Blvd.', 4, 4, 89119);

insert into Departure values(Null, '2016-06-01', '12:00:00', 1, 1);
insert into Departure values(Null, '2016-06-01', '14:00:00', 1, 1);
insert into Departure values(Null, '2016-05-07', '12:00:00', 1, 1);
insert into Departure values(Null, '2016-05-21', '12:00:00', 1, 1);
insert into Departure values(Null, '2016-05-07', '12:00:00', 3, 3);
insert into Departure values(Null, '2016-05-21', '12:00:00', 3, 3);
insert into Departure values(Null, '2016-05-07', '09:00:00', 1, 1);
insert into Departure values(Null, '2016-05-07', '09:00:00', 3, 3);
insert into Departure values(Null, '2016-05-21', '09:00:00', 1, 1);
insert into Departure values(Null, '2016-05-21', '09:00:00', 3, 3);

insert into Arrival values(Null, '2016-06-01', '16:00:00', 2, 2);
insert into Arrival values(Null, '2016-06-01', '17:30:00', 3, 3);
insert into Arrival values(Null, '2016-06-01', '17:00:00', 4, 4);
insert into Arrival values(Null, '2016-05-07', '12:00:00', 1, 1);
insert into Arrival values(Null, '2016-05-21', '12:00:00', 1, 1);
insert into Arrival values(Null, '2016-05-07', '12:00:00', 3, 3);
insert into Arrival values(Null, '2016-05-21', '12:00:00', 3, 3);
insert into Arrival values(Null, '2016-05-07', '15:00:00', 1, 1);
insert into Arrival values(Null, '2016-05-07', '15:00:00', 3, 3);
insert into Arrival values(Null, '2016-05-21', '15:00:00', 1, 1);
insert into Arrival values(Null, '2016-05-21', '15:00:00', 3, 3);


insert into Flight values(Null, '3072', 800.00, 4, 7, 6);
insert into Flight values(Null, '2114', 379.00, 5, 6, 10);
insert into Flight values(Null, '2164', 462.00, 5, 3, 9);
insert into Flight values(Null, '9207', 714.00, 6, 10, 5);

insert into Flight values(Null, '2000', 300.00, 1, 1, 1);
insert into Flight values(Null, '3000', 300.00, 2, 2, 2);
insert into Flight values(Null, '4000', 200.00, 3, 2, 3);
--insert into Flight values(Null, '5249', 150.00, 4, 7, 4);
insert into Flight values(Null, '1863', 220.00, 4, 7, 11);
insert into Flight values(Null, '4986', 642.00, 4, 7, 10);
insert into Flight values(Null, '7078', 398.00, 6, 8, 4);
insert into Flight values(Null, '4443', 508.00, 6, 8, 6);
insert into Flight values(Null, '1117', 467.00, 6, 8, 11);
insert into Flight values(Null, '8223', 577.00, 3, 3, 11);
insert into Flight values(Null, '2433', 379.00, 5, 6, 7);

insert into FlightUser values(2, 2);
insert into FlightUser values(3, 2);

insert into Payment values(Null, '2016-05-01', 500.00, 2, 2);
insert into Payment values(Null, '2016-05-01', 500.00, 3, 2);

insert into PaymentType values(Null, 'Credit');
insert into PaymentType values(Null, 'Debit');