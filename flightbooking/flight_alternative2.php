<?php
session_start();
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

function dbConnect()
{
    $user = 'root';
    $password = 'root';
    $db = 'FlightDB';
    $host = 'localhost';
    $port = 3306;
    
    $db = mysqli_connect($host, $user, $password, $db) or 
        die("<br />Error: unable to connect to db<br />");
    
    /*$link = mysql_connect(
       "$host:$port", 
       $user, 
       $password
    );
    $db_selected = mysql_select_db(
       $db, 
       $link
    );*/
    
    return $db;
}

echo "<!DOCTYPE html>\n"; 
echo "<html lang=\"en\">\n"; 
echo "<head>\n"; 
echo "  <title>Flight Booking </title>\n"; 
echo "  <meta name=\"description\" content=\"\" />\n"; 
echo "  <meta name=\"keywords\" content=\"\" />\n"; 
echo "  <meta charset=\"utf-8\" /><link rel=\"icon\" href=\"favicon.png\" />\n"; 
echo "  <meta content=\"eTravel.flights.searches.new\" name=\"js-namespace\">\n"; 
echo "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\"> \n"; 
echo "  <link rel=\"stylesheet\" href=\"css/jquery-ui.css\">\n"; 
echo "  <link href=\"css/calendar.css\" media=\"all\" rel=\"stylesheet\" type=\"text/css\">\n"; 
echo "  <link rel=\"stylesheet\" href=\"css/jquery-ui.css\">\n"; 
echo "  <link rel=\"stylesheet\" href=\"css/jquery.formstyler.css\">\n"; 
echo "  <link rel=\"stylesheet\" href=\"css/style.css\" />\n"; 
echo "  <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>\n"; 
echo "  <link href='http://fonts.googleapis.com/css?family=Lora:400,400italic' rel='stylesheet' type='text/css'>    \n"; 
echo "  <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,500,700' rel='stylesheet' type='text/css'>  \n"; 
echo "  <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>\n"; 
echo "  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>\n"; 
echo "</head>  \n"; 
echo "<body>  \n"; 
echo "<!-- // authorize // -->\n"; 
echo "	<div class=\"overlay\"></div>\n"; 
echo "	<div class=\"autorize-popup\">\n"; 
echo "		<div class=\"autorize-tabs\">\n"; 
echo "			<a href=\"#\" class=\"autorize-tab-a current\">Sign in</a>\n"; 
echo "			<a href=\"#\" class=\"autorize-tab-b\">Register</a>\n"; 
echo "			<a href=\"#\" class=\"autorize-close\"></a>\n"; 
echo "			<div class=\"clear\"></div>\n"; 
echo "		</div>\n"; 
echo "		<section class=\"autorize-tab-content\">\n"; 
echo "			<div class=\"autorize-padding\">\n"; 
echo "				<h6 class=\"autorize-lbl\">Welcome! Login in to Your Accont</h6>\n"; 
echo "				<input type=\"text\" value=\"\" placeholder=\"Name\">\n"; 
echo "				<input type=\"text\" value=\"\" placeholder=\"Password\">\n"; 
echo "				<footer class=\"autorize-bottom\">\n"; 
echo "					<button class=\"authorize-btn\">Login</button>\n"; 
echo "					<a href=\"#\" class=\"authorize-forget-pass\">Forgot your password?</a>\n"; 
echo "					<div class=\"clear\"></div>\n"; 
echo "				</footer>\n"; 
echo "			</div>\n"; 
echo "		</section>\n"; 
echo "		<section class=\"autorize-tab-content\">\n"; 
echo "			<div class=\"autorize-padding\">\n"; 
echo "				<h6 class=\"autorize-lbl\">Register for Your Account</h6>\n"; 
echo "				<input type=\"text\" value=\"\" placeholder=\"Name\">\n"; 
echo "				<input type=\"text\" value=\"\" placeholder=\"Password\">\n"; 
echo "				<footer class=\"autorize-bottom\">\n"; 
echo "					<button class=\"authorize-btn\">Registration</button>\n"; 
echo "					<div class=\"clear\"></div>\n"; 
echo "				</footer>\n"; 
echo "			</div>\n"; 
echo "		</section>\n"; 
echo "	</div>\n"; 
echo "<!-- \\ authorize \\-->\n"; 
echo "\n"; 
echo "<header id=\"top\">\n"; 
echo "	<div class=\"header-a\">\n"; 
echo "		<div class=\"wrapper-padding\">			\n"; 
echo "			\n"; 
echo "			<div class=\"header-account\">\n"; 
echo "				<a href=\"#\">Login/Register</a>\n"; 
echo "			</div>\n"; 
echo "			\n"; 


///IF LOGGED IN 
echo "            <div class=\"header-viewed\">\n"; 
echo "				<a href=\"account.php\" class=\"header-viewed-btn\">My Account</a>\n"; 
//THIS IS WHERE WE HAVE MYACCOUNT PAGE IF LOGGED IN
echo "				\n"; 
echo "			</div>\n"; 
echo "			\n"; 
echo "			<div class=\"clear\"></div>\n"; 
echo "		</div>\n"; 
echo "	</div>\n"; 
echo "	\n"; 
echo "		\n"; 
echo "		\n"; 
echo "			<div class=\"clear\"></div>\n"; 
echo "		</div>\n"; 
echo "	</div>	\n"; 
echo "</header>\n"; 
echo "\n"; 
echo "\n"; 
echo "<!-- main-cont -->\n"; 
echo "<div class=\"main-cont\">\n"; 
echo "  <div class=\"body-wrapper\">\n"; 
echo "    <div class=\"wrapper-padding\">\n"; 
echo "    <div class=\"page-head\">\n"; 
echo "      <div class=\"page-title\">Flights - <span>Search</span></div>\n"; 
echo "      <div class=\"breadcrumbs\">\n"; 
echo "        <a href=\"#\">Home</a> / <span>Flight Search page</span>\n"; 
echo "      </div>\n"; 
echo "      <div class=\"clear\"></div>\n"; 
echo "    </div>\n"; 
echo "    <div class=\"two-colls\">\n"; 
echo "      <div class=\"two-colls-left\">\n"; 
echo "        \n"; 
echo "        <div class=\"srch-results-lbl fly-in\">\n"; 

$img = "<img src='img/airplaneSilhouetteBlueCircle.png' alt='OSBK Logo' width='97' height='53' />";
$departure = "";
$deptCityID = -1;
$deptDate = "";
$arrival = "";
$arvCityID = -1;
$retDeptDate = "";

$duration = "";
$pricePerPerson = 0;

if(isset($_POST['departure']))
{
    $arrival = htmlentities($_POST['departure']);
    //echo "<br />departure = $departure <br />";
}
  
if(isset($_POST['arrival']))
{
    $departure = htmlentities($_POST['arrival']);
    //echo "<br />arrival = $arrival <br />";
}

if(isset($_POST['deptDate']))
{
    $deptDate = htmlentities($_POST['deptDate']);
    //echo "<br />deptDate = $deptDate <br />";
}

if(isset($_POST['retDate']))
{
    $retDate = htmlentities($_POST['retDate']);
    //echo "<br />retDate = $retDate <br />";
}

if(isset($_POST['people']))
{
	$people= htmlentities($_POST['people']);
	//echo "<br />people = $people <br />";
}

if(isset($_POST['deptFlightID']))
{
    $deptFlightID = htmlentities($_POST['deptFlightID']);
}

$deptCityID = -1;
$arvCityID = -1;
$flights = array();
$duration = "";
$pricePerPerson = 0;

$db = dbConnect();

// get departure cityID
$query = "SELECT cityID FROM City WHERE cityName = '$departure';";
$result = mysqli_query($db, $query) or die("<br />Error: unable to obtain departure cityID<br />");
if($result) $deptCityID = mysqli_fetch_row($result)[0];

// get arrival cityID
$query = "SELECT cityID FROM City WHERE cityName = '$arrival';";
$result = mysqli_query($db, $query) or die("<br />Error: unable to obtain arrival cityID<br />");
if($result) $arvCityID = mysqli_fetch_row($result)[0];

// get all the flights matching the search criteria
$query = "
SELECT Flight.flightID, Flight.flightNumber, Flight.price,
  Flight.aircraftID, Flight.departureID, Flight.arrivalID,
  deptArvIDs.departureTime, deptArvIDs.arrivalTime
FROM Flight
INNER JOIN
    (
        SELECT Departure.departureID, Departure.departureTime, Arrival.arrivalID, Arrival.arrivalTime
        FROM Departure, Arrival
        WHERE
          Departure.departureDate = '$retDate' AND
          Departure.cityID = $deptCityID AND
          Arrival.cityID = $arvCityID AND
          ((DATEDIFF(Departure.departureDate, DATE_ADD(Arrival.arrivalDate, INTERVAL 1 DAY)) = 0) OR
           (Departure.departureDate = Arrival.arrivalDate AND
             Arrival.arrivalTime > Departure.departureTime))
        ORDER BY Departure.departureDate, Departure.departureTime
    ) deptArvIDs
    ON
      Flight.departureID = deptArvIDs.departureID AND
      Flight.arrivalID = deptArvIDs.arrivalID;";
$result = mysqli_query($db, $query) or die("<br />Error: unable to obtain flights<br />");

while($row = mysqli_fetch_assoc($result))
{
    array_push($flights, $row);
}

echo "          <span>". count($flights) ." ";

if(count($flights) == 1) echo "result";
else echo "results";

echo " found.</span>\n";
echo "        </div> \n";
echo "	       \n";
echo "        <div class=\"side-block fly-in\">\n";
echo "          <div class=\"side-block-search\">\n";
echo "            <div class=\"page-search-p\">\n";
echo "						<!-- // -->\n";
echo "						<div class=\"srch-tab-line\">\n";
echo "							<div class=\"srch-tab-left\">\n";
echo "\n";
echo "                            \n";

echo"        <form name='search' action='".$_SERVER['PHP_SELF']."' method='post'> ";

echo "								<label>From</label>\n";
echo "								<div class=\"input-a\"><input type=\"text\" name =\"departure \" value=\"$departure\"></div>	\n";
echo "							</div>\n";
    echo "							<div class=\"srch-tab-right\">\n";
    echo "								<label>To</label>\n";
    echo "								<div class=\"input-a\"><input type=\"text\"name=\"arrival\" value=\"$arrival\"            placeholder=\"London\"></div>	\n";
    echo "							</div>\n";
    echo "							<div class=\"clear\"></div>\n";
    echo "						</div>\n";
    echo "						<!-- \\ -->\n";
    echo "						<!-- // -->\n";
    echo "						<div class=\"srch-tab-line\">\n";
    echo "							<div class=\"srch-tab-left\">\n";
    echo "								<label>Departure</label>\n";
    echo "								<div class=\"input-a\"><input type=\"text\" value=\"$retDate\" class=\"date-inpt\" placeholder=\"mm/dd/yy\"> <span class=\"date-icon\"></span></div>	\n";
    echo "							</div>\n";


    echo"<div class='srch-tab-right'>";
    echo"								<label>Return</label>";
    echo"								<div class='input-a'><input type='text' value='' class='date-inpt' placeholder='mm/dd/yy'> <span class='date-icon'></span></div>	";
    echo"						</div>";


    echo "							<div class=\"clear\"></div>\n";
    echo "						</div>\n";
    echo "						<!-- \\ -->\n";
            echo "						<div class=\"srch-tab-line no-margin-bottom\">\n";
    echo "							<div class=\"srch-tab-left transformed\">\n";
    echo "								<label>Passengers</label>\n";
    echo "								<div class=\"select-wrapper\">\n";
    echo "								<select class=\"custom-select\" name='people'>\n";
    echo "									<option>$people</option>\n";
    echo "									<option>1</option>\n";
            echo "									<option>2</option>\n";
    echo "									<option>3</option>\n";
    echo "									<option>4</option>\n";
    echo "								</select>\n";
    echo "								</div>	\n";
            echo "							</div>\n";

            echo "							<div class=\"clear\"></div>\n";
    echo "						</div>\n";
                    echo "						<!-- \\ -->	\n";
    echo "\n";
    echo"							 </form> \n";
    echo "             			<button class=\"srch-btn\">Search</button> \n";

    echo "    					</div>  \n";
    echo "                   \n";
    echo "          </div>          \n";
    echo "        </div>\n";
    echo "\n";

            echo "        \n";
                    echo "      </div>\n";
    echo "      <div class=\"two-colls-right\">\n";
    echo "        <div class=\"two-colls-right-b\">\n";
    echo "          <div class=\"padding\">\n";
    echo "          \n";
    /*echo "            <div class=\"catalog-head fly-in\">\n";
                    echo "              <label>Sort results by:</label>\n";
                    echo "              <div class=\"search-select\">\n";
                    echo "    							<select>\n";
                    echo "    								<option>Name</option>\n";
                    echo "    								<option>Name</option>\n";
                    echo "    								<option>Name</option>\n";
                    echo "    								<option>Name</option>\n";
                    echo "    								<option>Name</option>\n";
                    echo "    							</select>\n";
                    echo "    					</div>\n";
                    echo "              <div class=\"search-select\">\n";
                    echo "    							<select>\n";
                    echo "    								<option>Price</option>\n";
                    echo "    								<option>Price</option>\n";
                    echo "    								<option>Price</option>\n";
                    echo "    								<option>Price</option>\n";
                    echo "    								<option>Price</option>\n";
                    echo "    							</select>\n";
                    echo "    					</div>\n";
                    echo "              <div class=\"search-select\">\n";
                    echo "    							<select>\n";
                    echo "    								<option>Duration</option>\n";
                    echo "    								<option>Duration</option>\n";
                    echo "    								<option>Duration</option>\n";
                    echo "    								<option>Duration</option>\n";
                    echo "    								<option>Duration</option>\n";
                    echo "    							</select>\n";
                    echo "    					</div>\n";
                    echo "              <a href=\"#\" class=\"show-list\"></a>              \n";
                    echo "			  <a class=\"show-thumbs chosen\" href=\"#\"></a> \n";
                    echo "              <div class=\"clear\"></div>\n";
                    echo "            </div>\n";
                    echo "            \n";*/

if(count($flights) == 0)
{
     echo "No flights found matching your criteria.";
}    
else
{
    foreach($flights as $flight)
    {
    
        $retFlightID = $flight['flightID'];
        $flightNum = $flight['flightNumber'];
        $aircraftID = $flight['aircraftID'];
        $departureTime = $flight['departureTime'];
        $arrivalTime = $flight['arrivalTime'];
        $price = $flight['price'];
        
        $duration = $arrivalTime - $departureTime;
        $total = $price * $people;
        $pricePerPerson = $price;
        
        $modelName = "";
        $query = "SELECT Model.modelName FROM Aircraft, Model 
                WHERE Aircraft.modelID = Model.modelID 
                ORDER BY ModelName; ";
        $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain modelName<br />");
        if($result) $modelName = mysqli_fetch_row($result)[0];
    
        echo "            <div class=\"catalog-row alternative\">\n"; 
        echo "				\n"; 
        echo "				<!-- // alt-flight // -->\n"; 
        echo "					<div class=\"alt-flight fly-in\">\n"; 
        echo "						<div class=\"alt-flight-a\">\n"; 
        echo "							<div class=\"alt-flight-l\">\n"; 
        echo "  								<div class=\"alt-flight-lb\">\n"; 
        echo "									<div class=\"alt-center\">\n"; 
        echo "										<div class=\"alt-center-l\">\n"; 
        echo "  											<div class=\"alt-center-lp\">\n"; 
        echo "  											<div class=\"alt-logo\">\n"; 
        echo "  												$img</a>\n"; 
        echo "											</div>\n"; 
        echo "											</div>\n"; 
        echo "										</div>\n"; 
        echo "										<div class=\"alt-center-c\">\n"; 
        echo "  										<div class=\"alt-center-cb\">\n"; 
        echo "    										<div class=\"alt-center-cp\">\n";                
            echo "<form name=\"deptFlight\" action=\"flight_booking.php\" method=\"post\">";
            echo "    											<div class=\"alt-lbl\">".$departure." - ".$arrival."</div>\n"; 
            echo "    											<div class=\"alt-info\">One way trip</div>\n"; 
            echo "    											<div class=\"alt-devider\"></div>\n"; 
            echo "							  					<div class=\"flight-line-b\">\n"; 
            echo "                                					<b>details</b>\n"; 
            echo"  <span> Seats go quick!</span>";
            
            echo "                              					</div>    											"; 
            echo "    											<div class=\"alt-data-i alt-departure\">\n"; 
            echo "    												<b>Departure</b>\n"; 
            echo "    												<span>".$departureTime."</span>\n"; 
            echo "    											</div>\n"; 
            
            echo "    											<div class=\"alt-data-i alt-arrival\">\n"; 
            echo "    												<b>Arrival</b>\n"; 
            echo "    												<span>".$arrivalTime."</span>\n"; 
            echo "    											</div>\n";    
            
            echo "    											<div class=\"alt-data-i alt-time\">\n"; 
            echo "    												<b>time</b>\n"; 
            echo "    												<span>".$duration."H 0M</span>\n"; 
            echo "    											</div>\n"; 
            
            echo "    											<div class=\"alt-data-i alt-time\">\n"; 
            echo "    												<b>Date</b>\n"; 
            echo "    												<span>".$deptDate."</span>\n"; 
            echo "    											</div>\n"; 
            
            
            
            
            
            
            echo "    											<div class=\"clear\"></div>\n"; 
            echo "    										</div>\n"; 
            echo "  										</div>\n"; 
            echo "  										<div class=\"clear\"></div>\n"; 
            echo "										</div>\n"; 
            echo "									</div>\n"; 
            echo "									<div class=\"clear\"></div>\n"; 
            echo "  								</div>\n"; 
            echo "  								<div class=\"clear\"></div>	\n"; 
            echo "							</div>\n"; 
            echo "						</div>\n"; 
            echo "						<div class=\"alt-flight-lr\">\n"; 
            echo "  						<div class=\"padding\">\n"; 
            echo "    						<div class=\"flt-i-price\">$".$price."</div>\n"; 
            echo "    						<div class=\"flt-i-price-b\">avg/person</div>\n";
            echo "    						<div class=\"flt-i-price\">$".$total."</div>\n"; 
            echo "    						<div class=\"flt-i-price-b\">total</div>\n";
            echo"                            <input type=\"hidden\" name='departure' value='$departure' >";
            echo "                           <input type=\"hidden\" name='arrival' value='$arrival' >";
             echo"		`						<input type='hidden' name='deptDate' value='$deptDate' >";
              echo"      						<input type='hidden' name='retDate' value='$retDate' >";
               echo"                             <input type='hidden' name='people' value='$people' >";
                  echo"                             <input type='hidden' name='deptFlightID' value='$deptFlightID' >";
                  echo"                             <input type='hidden' name='retFlightID' value='$retFlightID' >";
             	echo"							<input type='submit' name='selectRet' value='Select Now' class='cat-list-btn'/> </form>";
            								
            								
            //echo "    						<a class=\"cat-list-btn\" href=\"flight_alternative2.php\">select now</a>          \n"; 
            echo "  						</div>\n"; 
            echo "  						<div class=\"clear\"></div>\n"; 
            echo "						</div>\n"; 
            echo "						<div class=\"clear\"></div>\n"; 
            
            
            
            echo "						<div class=\"alt-details\">\n"; 
            
            echo "							<div class=\"alt-details-i\">\n"; 
            echo "								<b>Flight ".$flightNum." ".$modelName." (Jet) Economy</b>\n"; 
            echo "								<span>Operated by SKBN Airlines</span>\n"; 
            echo "							</div>\n"; 
            echo "							<div class=\"clear\"></div>\n"; 
            echo "						</div>\n"; 
            echo "					</div>\n"; 
            echo "				<!-- \\ alt-flight \\ -->\n"; 
    }
}
mysqli_close($db);

echo "<!-- // scripts // -->\n"; 
echo "  <script src=\"js/jquery-1.11.3.min.js\"></script>\n"; 
echo "  <script src=\"js/jqeury.appear.js\"></script>  \n"; 
echo "  <script src=\"js/owl.carousel.min.js\"></script>\n"; 
echo "  <script src=\"js/bxSlider.js\"></script>\n"; 
echo "  <script src=\"js/jquery.formstyler.js\"></script>   \n"; 
echo "  <script src=\"js/custom.select.js\"></script> \n"; 
echo "  <script src=\"js/jquery-ui.min.js\"></script>  \n"; 
echo "  <script type=\"text/javascript\" src=\"js/twitterfeed.js\"></script>\n"; 
echo "  <script src=\"js/script.js\"></script>\n"; 
echo "  <script>\n"; 
echo "  	$(document).ready(function(){\n"; 
echo "	  'use strict';\n"; 
echo "      (function($) {\n"; 
echo "        $(function() {\n"; 
echo "          $('input:checkbox,input:radio,.search-engine-range-selection-container input:radio').styler();\n"; 
echo "        })\n"; 
echo "      })(jQuery);  \n"; 
echo "	  \n"; 
echo "		var slider_range = $(\"#slider-range\");\n"; 
echo "		var ammount_from = $(\"#ammount-from\");\n"; 
echo "		var ammount_to = $(\"#ammount-to\");\n"; 
echo "		\n"; 
echo "		$(function() {\n"; 
echo "			slider_range.slider({\n"; 
echo "			  range: true,\n"; 
echo "			  min: 0,\n"; 
echo "			  max: 1500,\n"; 
echo "			  values: [ 275, 1100 ],\n"; 
echo "			  slide: function( event, ui ) {\n"; 
echo "				ammount_from.val(ui.values[0]+'$');\n"; 
echo "				ammount_to.val(ui.values[1]+'$');\n"; 
echo "			  }\n"; 
echo "			});\n"; 
echo "			ammount_from.val(slider_range.slider(\"values\",0)+'$');\n"; 
echo "			ammount_to.val(slider_range.slider(\"values\",1)+'$');\n"; 
echo "		});\n"; 
echo "\n"; 
echo "        $(\".side-time\").each(function(){\n"; 
echo "          var $this = $(this);\n"; 
echo "          $this.find('.time-range').slider({\n"; 
echo "              range: true,\n"; 
echo "              min: 0,\n"; 
echo "              max: 24,\n"; 
echo "              values: [ 3, 20 ],\n"; 
echo "              slide: function( event, ui ) {\n"; 
echo "                $this.find(\".time-from\").text(ui.values[0]);\n"; 
echo "                $this.find(\".time-to\").text(ui.values[1]);\n"; 
echo "              }\n"; 
echo "          });\n"; 
echo "          $(this).find(\".time-from\").text($this.find(\".time-range\").slider(\"values\",0));\n"; 
echo "          $(this).find(\".time-to\").text($this.find(\".time-range\").slider(\"values\",1));\n"; 
echo "        });\n"; 
echo "  	});\n"; 
echo "  </script>\n"; 
echo "<!-- \\ scripts \\ --> \n"; 
echo " \n"; 
echo "</body>  \n"; 
echo "</html> \n";
?>