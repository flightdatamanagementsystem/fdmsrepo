<?php
/* Kristin Hamilton */
// utilFunctions.php
//
// printPageHeader(), printPageFooter(), printContactBox()

// dont use yet-need to test it!
function doHtmlEntitiesOnNestedAssocArray($object)
{
    if(!is_array($object))
    {
        return htmlentities($object);
    }    
    else
    {
        foreach($object as $element)
        {
            $element = doHtmlEntitiesOnNestedAssocArray($object);
        }
    }
}

/* prints drop down options from $start to $end, using $defaultText selected, unless 
 *   $selectedOption specified and > -1
 * 
 * examples: 
 *   printDropDownOptions_int(5, 7, 'Choose a number between 5 and 7', -1)
 *     will print:
 *       "<option selected>'Select number of fingers you have'</option>
 *        <option>05</option>
 *        <option>06</option>
 *        <option>07</option>" 
 *   printDropDownOptions_int(5, 7, 'Choose a number between 5 and 7', 7)
 *     will print:
 *       "<option>'Select number of fingers you have'</option>
 *        <option>05</option>
 *        <option>06</option>
 *        <option selected>07</option>"  
 */
function printDropDownOptions_ints($start, $end, $defaultText, $selectedOption)
{
    $optionTemplate = "<option%s>%d</option>";
    $optionText = "";
    $dropDownOptions = "";
    
    $dropDownOptions .= "<option";
    if($selectedOption == -1){ $dropDownOptions .= " selected"; }
    $dropDownOptions .= ">$defaultText</option>";
    
    for($i = $start; $i < $end + 1; $i++)
    {
        $optionText = $i;
        if($selectedOption > -1 && $i == $selectedOption)
        {
            $dropDownOptions .= sprintf($optionTemplate, " selected", $i);
        }
        else
        {
            $dropDownOptions .= sprintf($optionTemplate, "", $i);
        }        
    }
    echo $dropDownOptions;
}

/* example: 
 *     $pageTitle = "Alternative";
 *     $navigationPageTitle = "Flight Alternative Page"
*/
function printPageHeader($pageTitle, $navigationPageTitle) 
{
    $self = $_SERVER['PHP_SELF'];

    if(isset($_POST['logout']))
    {
        echo "Logged Out";
        $_SESSION['status']=-1;
        session_destroy();
    }
    else if(isset($_POST['login']))
    {
        $username = $_POST['username'];
        $password = $_POST['password'];
    
        $result = mysql_query("select * from User where username='" . $username . "' and password='". $password . "'");
        if (!$result) {
            die('Invalid query: ' . mysql_error());
        }
    
        while ($row = mysql_fetch_assoc($result)) {
            $_SESSION["username"]=$row["username"];
            $_SESSION["firstName"]=$row["firstName"];
            $_SESSION["lastName"]=$row["lastName"];
            $_SESSION["status"]=$row["status"];
        }
    
        if($_SESSION["status"]=="1")
        {
            header("Location: index.php");
        }
        else
        {
            echo "</br> invalid session... the current session is" . $_SESSION["status"];
        }
    }
    
    echo 
"<!DOCTYPE html>
<html lang='en'>
<head>
    <title>$pageTitle</title>
    <meta name='description' content='' />
    <meta name='keywords' content='' />
    <meta charset='utf-8' /><link rel='icon' href='../flightbooking/favicon.png' />
    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'>
    <script src='../booking/js/booking.js'></script>  
    <link rel='stylesheet' href='../flightbooking/css/jquery.formstyler.css'>
    <link rel='stylesheet' href='../flightbooking/css/style.css' />
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lora:400,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
            
<body class='index-page'>  
<!-- // authorize // -->
	<div class='overlay'></div>
	<div class='autorize-popup'>
		<div class='autorize-tabs'>
			<a href='#' class='autorize-tab-a current'>Sign in</a>
			<a href='#' class='autorize-close'></a>
			<div class='clear'></div>
		</div>
		<section class='autorize-tab-content'>
			<div class='autorize-padding'>
				<h6 class='autorize-lbl'>Welcome! Login in to Your Account</h6>
				<form name='test' action='$self' method='post'>
				<input type='text' name='username' value='' placeholder='username'>
				<input type='text' name='password' value='' placeholder='password'>
				
				<footer class='autorize-bottom'>
					<input type='submit' name='login' class='authorize-btn' value='Login'>
					</form>
					<a href='#' class='authorize-forget-pass'>Forgot your password?</a>
					<div class='clear'></div>
				</footer>
			</div>
		</section>
	</div>
<!-- \\ authorize \\-->

<header id='top'>
	<div class='header-a'>
		<div class='wrapper-padding'>			
			<div class='header-viewed'>
				<a href='registerform.php' class='header-viewed-btn'>Register</a>
			</div>";
		
    if(isset($_SESSION['status']) && $_SESSION['status'] == 1)
    { 		
		echo 
		"<div class='header-viewed'>		
		    <form name='register' action='$self' method='post'>
		        <input type='submit' class='header-viewed-btn' name='logout' value='Logout'></br>
	        </form>
		</div>";
	}	

	echo 
	"<div class='header-account'>
	    <a href='#'>Login</a>
	</div>
			
    <!-- IF LOGGED IN-->";
	
    if(isset($_SESSION['status']) && $_SESSION['status'] == 1)
    {
        echo "<div class='header-viewed'>
		<a href='account.php' class='header-viewed-btn'>My Account</a>
        </div>";
    }
           
    echo "  <div class='clear'></div>
		</div>
	</div>
	<div class='header-b'>
		<div class='wrapper-padding'>
			<div class='header-right'>
				<div class='hdr-srch-overlay'>
					</div>
				</div>	
			<div class='clear'></div>
		</div>
	</div>	
</header>";

    echo
"<!-- main-cont -->
    <div class='main-cont'>
        <div class='body-wrapper'>
            <div class='wrapper-padding'>
                <div class='page-head'>
                    <div class='page-title'>Flights - <span>$pageTitle</span></div>
                    <div class='breadcrumbs'>
                        <a href='../flightbooking/index.php'>Home</a> / <span>$navigationPageTitle</span>
                    </div>
                    <div class='clear'></div>
                </div>";
}

function printCheckoutColumn($booking)
{    
    $flight1 = $booking->getFlight1();
    $flight2 = $booking->getFlight2();
    $flightDeptDate1 = explode(", ", explode(" | ", $flight1->getFlightDepartureDate(true))[1]);
    $flightArvDate1 = explode(", ", explode(" | ", $flight1->getFlightArrivalDate(true))[1]);
    $flightDeptDate2 = explode(", ", explode(" | ", $flight2->getFlightDepartureDate(true))[1]);
    $flightArvDate2 = explode(", ", explode(" | ", $flight2->getFlightArrivalDate(true))[1]);
    
    echo
"               <div class='checkout-coll'>
                    <div class='checkout-head'>
                        <div class='checkout-headl'>
                             <img alt='aircraft' width='93' height='75' src='../flightbooking/img/airplaneSilhouetteBlueCircle.png'>
                        </div>
                        <div class='checkout-headr'>
                            <div class='checkout-headrb'>
                                <div class='checkout-headrp'>
                                    <div class='chk-left'>
                                        <div class='chk-lbl'><div class='page-title'>".
                                        $flight1->getFlightDepartureAirportCity() ."-". 
                                        $flight1->getFlightArrivalAirportCity() ."</b></div></div>
                                        <div class='chk-lbl-a'>TWO-WAY FLIGHT</div>";
    echo "
                                    
                                    </div>";
    
    echo
"                                    <div class='clear'></div>
                                </div>
                            </div>
                            <div class='clear'></div>
                        </div>
                    </div>
    
                    <div class='chk-lines'>
                        <span style='color:#FF7200;'><b>LEAVING</b></span>
                        <div class='chk-line chk-fligth-info'>
                            <div class='chk-departure'>
                                <span>Departure</span>
                                <b>". $flight1->getFlightDepartureTime() ."<br>".
                                $flightDeptDate1[0] ."</b>
                            </div>
                            <div class='chk-fligth-devider'></div>
                            <div class='chk-fligth-time'></div>
                            <div class='chk-fligth-devider'></div>
                            <div class='chk-arrival'>
                                <span>Arrival</span>
                                <b>". $flight1->getFlightArrivalTime() ."<br>".
                                $flightArvDate1[0] ."</b>
                            </div>
                            <div class='clear'></div>
                        </div>                        
                    </div>
                
                    <div class='chk-details'>
                        Details
                        <div class='chk-detais-row'>";
                            echo
                            "<div class='chk-line'>
                                <span class='chk-l'>Seats</span>
                                <span class='chk-r'>". $flight1->getFlightTravelerCount() ."</span>
                                <div class='clear'></div>
                            </div>
                            <div class='chk-line'>
                                <span class='chk-l'>Price per seat</span>
                                <span class='chk-r'>\$". round($flight1->getFlightPrice(), 2) ."</span>
                                <div class='clear'></div>
                            </div>
                        </div>                   
                    </div>

                    <div class='chk-lines'>
                        <span style='color:#FF7200;'><b>RETURNING</b></span>
                        <div class='chk-line chk-fligth-info'>
                            <div class='chk-departure'>
                                <span>Departure</span>
                                <b>". $flight2->getFlightDepartureTime() ."<br>".
                                $flightDeptDate2[0] ."</b>
                            </div>
                            <div class='chk-fligth-devider'></div>
                            <div class='chk-fligth-time'></div>
                            <div class='chk-fligth-devider'></div>
                            <div class='chk-arrival'>
                                <span>Arrival</span>
                                <b>". $flight2->getFlightArrivalTime() ."<br>".
                                $flightArvDate2[0] ."</b> 
                            </div>
                            <div class='clear'></div>
                        </div>
                    </div>
                                        
                    <div class='chk-details'>
                        Details
                        <div class='chk-detais-row'>";
                            echo
                            "<div class='chk-line'>
                                <span class='chk-l'>Seats</span>
                                <span class='chk-r'>". $flight2->getFlightTravelerCount() ."</span>
                                <div class='clear'></div>
                            </div>
                            <div class='chk-line'>
                                <span class='chk-l'>Price per seat</span>
                                <span class='chk-r'>\$". round($flight2->getFlightPrice()) ."</span>
                                <div class='clear'></div>
                            </div>
                        </div>                 
                    </div>
                                    
                   <div class='chk-details'>
                        <b>TOTALS</b>
                        <div class='chk-detais-row'>";
                            echo
                            "<div class='chk-line'>
                                <span class='chk-l'>Subtotal</span>
                                <span class='chk-r'>\$". round($booking->getBasePriceTotal()) ."</span>
                                <div class='clear'></div>
                            </div>
                            <div class='chk-line'>
                                <span class='chk-l'>Taxes &amp; Fees</span>
                                <span class='chk-r'>\$". round($booking->getTaxesAndFeesTotal()) ."</span>
                                <div class='clear'></div>
                            </div>
                        </div>
                        <div class='chk-total'>
                            <div class='chk-total-l'>Total Price</div>
                            <div class='chk-total-r'>\$". round($booking->getFinalPriceTotal()) ."</div>
                            <div class='clear'></div>
                        </div>                    
                    </div>             
                                    
                </div>";
}

function printPageFooter()
{
    echo     
    "<footer class='footer-a'>
    <div class='wrapper-padding'>
    <div class='section'>
    <div class='footer-lbl'>Get In Touch</div>
			<div class='footer-adress'>Address: 58911 Lepzig Hore,<br />85000 Vienna, Austria</div>
    			<div class='footer-phones'>Telephone: +1 777 55-32-21</div>
    			<div class='footer-email'>E-mail: contacts@miracle.com</div>
			<div class='footer-skype'>Skype: angelotours</div>
		</div>
    		<div class='section'>
    		<div class='footer-lbl'>Featured deals</div>
    		<div class='footer-tours'>
			<!-- // -->
    		        <div class='footer-tour'>
    		        <div class='footer-tour-l'><a href='#'><img alt='' src='img/f-tour-01.jpg' /></a></div>
				<div class='footer-tour-r'>
    				<div class='footer-tour-a'>amsterdam tour</div>
    				<div class='footer-tour-b'>location: netherlands</div>
    				<div class='footer-tour-c'>800$</div>
				</div>
    				<div class='clear'></div>
    				</div>
    				<!-- \\ -->
			<!-- // -->
    				        <div class='footer-tour'>
    				        <div class='footer-tour-l'><a href='#'><img alt='' src='img/f-tour-02.jpg' /></a></div>
    				        <div class='footer-tour-r'>
    				        <div class='footer-tour-a'>Kiev tour</div>
					<div class='footer-tour-b'>location: ukraine</div>
    					<div class='footer-tour-c'>550$</div>
    					</div>
    					        <div class='clear'></div>
    					        </div>
    					                <!-- \\ -->
    					                <!-- // -->
			<div class='footer-tour'>
    			<div class='footer-tour-l'><a href='#'><img alt='' src='img/f-tour-03.jpg' /></a></div>
    			<div class='footer-tour-r'>
    			        <div class='footer-tour-a'>vienna tour</div>
    			        <div class='footer-tour-b'>location: austria</div>
					<div class='footer-tour-c'>940$</div>
    					</div>
    					<div class='clear'></div>
    					</div>
    					<!-- \\ -->
    					</div>
    					</div>
    					<div class='section'>
    					<div class='footer-lbl'>Twitter widget</div>
    					<div class='twitter-wiget'>
    					<div id='twitter-feed'></div>
    					</div>
    					</div>
    					        <div class='section'>
    					        <div class='footer-lbl'>newsletter sign up</div>
    					                <div class='footer-subscribe'>
				<div class='footer-subscribe-a'>
    				<input type='text' placeholder='your email' value='' />
    				</div>
    				        </div>
    				        <button class='footer-subscribe-btn'>Sign up</button>
		</div>
    		</div>
    		<div class='clear'></div>
    		</footer>
    
    		<footer class='footer-b'>
    		<div class='wrapper-padding'>
		<div class='footer-left'>� Copyright 2015 by Weblionmedia. All rights reserved.</div>
		<div class='footer-social'>
    		<a href='#' class='footer-twitter'></a>
    			<a href='#' class='footer-facebook'></a>
    			<a href='#' class='footer-vimeo'></a>
    			<a href='#' class='footer-pinterest'></a>
    			<a href='#' class='footer-instagram'></a>
    		</div>
    		<div class='clear'></div>
    	</div>
    </footer>";
}

// <script src='../booking/js/booking.js'></script>  
function printScriptsBoilerplate()
{
    echo 
"    <!-- // scripts // -->
      <script src='js/jquery-1.11.3.min.js'></script>
      <script src='js/jqeury.appear.js'></script>  
      <script src='js/jquery-ui.min.js'></script> 
      <script src='js/owl.carousel.min.js'></script>
      <script src='js/bxSlider.js'></script>
      <script src='js/jquery.formstyler.js'></script> 
      <script src='js/custom.select.js'></script>   
      <script src='js/script.js'></script>
      <script>
          $(document).ready(function(){
            'use strict';
            (function($) {
                $(function() {
                    $('.checkbox input').styler({
                        selectSearch: true
                    });
                });
            })(jQuery);
          });
      </script>
    <!-- \\ scripts \\ -->";            
}

function printContactBox()
{
    echo 
    "<div class='h-help'>
         <div class='h-help-lbl'>Need Sparrow Help?</div>
         <div class='h-help-lbl-a'>We would be happy to help you!</div>
         <div class='h-help-phone'>2-800-256-124 23</div>
         <div class='h-help-email'>sparrow@mail.com</div>
     </div>";
}

function printReasonsToBookWithUs()
{
    echo 
    "<div class='h-reasons'>
            <div class='h-liked-lbl'>Reasons to Book with us</div>
            <div class='h-reasons-row'>
                <!-- // -->
                <div class='reasons-i'>
                    <div class='reasons-h'>
                        <div class='reasons-l'>
                            <img alt='' src='img/reasons-a.png'>
                        </div>
                        <div class='reasons-r'>
                            <div class='reasons-rb'>
                                <div class='reasons-p'>
                                    <div class='reasons-i-lbl'>Awesome design</div>
                                    <p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunt.</p>
                                </div>
                            </div>
                            <br class='clear'>
                        </div>
                    </div>
                    <div class='clear'></div>
                </div>
                <!-- \\ -->
                <!-- // -->
                <div class='reasons-i'>
					<div class='reasons-h'>
						<div class='reasons-l'>
					        <img alt='' src='img/reasons-b.png'>
						</div>
						<div class='reasons-r'>
					        <div class='reasons-rb'>
				                <div class='reasons-p'>
				                    <div class='reasons-i-lbl'>carefully handcrafted</div>
				                    <p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunt.</p>
				                </div>
			                </div>
			                <br class='clear'>
		                </div>
	                </div>
					<div class='clear'></div>
		        </div>
		        <!-- \\ -->
		        <!-- // -->
				<div class='reasons-i'>
					<div class='reasons-h'>
					   <div class='reasons-l'>
					       <img alt='' src='img/reasons-c.png'>
					   </div>
					   <div class='reasons-r'>
					       <div class='reasons-rb'>
					           <div class='reasons-p'>
						          <div class='reasons-i-lbl'>customer support</div>
						          <p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunt.</p>
						       </div>
						   </div>
						   <br class='clear'>
						</div>
					</div>
					<div class='clear'></div>
				</div>
				<!-- \\ -->
			</div>
		</div>";
}

/* linksBar: has the links for 'Home', 'Hotels', 'Flights', 'Tours', 'Pages', 'Portfolio', 
 * 'Blog', 'Features', 'Contacts'
 */
function printLinksBar()
{
    echo
    "            <div class='header-b'>
                <!-- // mobile menu // -->
                <div class='mobile-menu'>
                    <nav>
                        <ul>
                            <li><a class='has-child' href='#'>HOME</a>
                                <ul>
                                    <li><a href='index.html'>Home style one</a></li>
                                    <li><a href='index_02.html'>Home style two</a></li>
                                    <li><a href='index_03.html'>Home style three</a></li>
                                    <li><a href='index_04.html'>Home style four</a></li>
                                </ul>
                            </li>
                            <li><a class='has-child' href='#'>Hotels</a>
                                <ul>
                                    <li><a href='hotel_list.html'>Hotels standard list</a></li>
                                    <li><a href='hotel_simple_style.html'>Hotels simple style</a></li>
                                    <li><a href='hotel_detail_style.html'>Hotels detail style</a></li>
                                    <li><a href='hotel_detail.html'>Hotel item page</a></li>
                                    <li><a href='hotel_booking.html'>Hotel booking page</a></li>
                                    <li><a href='#'>booking complete page</a></li>
                                </ul>
                            </li>
                            <li><a class='has-child' href='#'>Flights</a>
                                <ul>
                                    <li><a href='flight_round_trip.html'>Flights round trip</a></li>
                                    <li><a href='flight_one_way.html'>flights one way trip</a></li>
                                    <li><a href='flight_alternative.html'>flights alternative style</a></li>
                                    <li><a href='flight_detail.html'>Flights detail page</a></li>
                                    <li><a href='flight_booking.html'>Flights booking page</a></li>
                                    <li><a href='booking_complete.html'>booking complete</a></li>
                                </ul>
                            </li>
                            <li><a class='has-child' href='#'>Tours</a>
                                <ul>
                                    <li><a href='tour_alternative.html'>Tours list style</a></li>
                                    <li><a href='tour_grid.html'>tours grid style</a></li>
                                    <li><a href='tour_simple.html'>Tours simple style</a></li>
                                    <li><a href='tour_detail.html'>Tour detail page</a></li>
                                    <li><a href='tour_booking.html'>tour booking page</a></li>
                                    <li><a href='booking_complete.html'>booking complete</a></li>
                                </ul>
                            </li>
                            <li><a class='has-child' href='#'>Pages</a>
                                <ul>
                                    <li><a href='about_us.html'>about us style one</a></li>
                                    <li><a href='services.html'>services</a></li>
                                    <li><a href='contacts.html'>contact us</a></li>
                                </ul>
                            </li>
                            <li><a class='has-child' href='#'>Portfolio</a>
                                <ul>
                                    <li><a href='portfolio_three_collumns.html'>Portfolio three columns</a></li>
                                    <li><a href='portfolio_four_collumns.html'>portfolio four columns</a></li>
                                    <li><a href='item_page.html'>item page</a></li>
                                    <li><a href='item_page_full_width.html'>Item page full width style</a></li>
                                </ul>
                            </li>
                            <li><a class='has-child' href='#'>Blog</a>
                                <ul>
                                    <li><a href='blog_with_sidebar.html'>Blog with sidebar</a></li>
                                    <li><a href='blog_masonry.html'>blog masonry style</a></li>
                                    <li><a href='standart_blog_post.html'>Blog post example</a></li>
                                </ul>
                            </li>
                            <li><a class='has-child' href='#'>Features</a>
                                <ul>
                                    <li><a href='typography.html'>typography</a></li>
                                    <li><a href='shortcodes.html'>shortcodes</a></li>
                                    <li><a href='interactive_elements.html'>interactive elements</a></li>
                                    <li><a href='cover_galleries.html'>cover galleries</a></li>
                                    <li><a href='columns.html'>columns</a></li>
                                </ul>
                            </li>
                            <li><a href='contacts.html'>CONTACS</a></li>
                        </ul>
                    </nav>
                </div>
                <!-- \\ mobile menu \\ -->

                <div class='wrapper-padding'>
                    <div class='header-logo'><a href='index.html'><img alt='' src='img/logo.png' /></a></div>
                    <div class='header-right'>
                        <div class='hdr-srch'>
                            <a href='#' class='hdr-srch-btn'></a>
                        </div>
                        <div class='hdr-srch-overlay'>
                            <div class='hdr-srch-overlay-a'>
                                <input type='text' value='' placeholder='Start typing...'>
                                <a href='#' class='srch-close'></a>
                                <div class='clear'></div>
                            </div>
                        </div>
                        <div class='hdr-srch-devider'></div>
                        <a href='#' class='menu-btn'></a>
                        <nav class='header-nav'>
                            <ul>
                                <li><a href='#'>Home</a>
                                    <ul>
                                        <li><a href='index.html'>Home style one</a></li>
                                        <li><a href='index_02.html'>Home style two</a></li>
                                        <li><a href='index_03.html'>Home style three</a></li>
                                        <li><a href='index_04.html'>Home style four</a></li>
                                    </ul>
                                </li>
                                <li><a href='#'>Hotels</a>
                                    <ul>
                                        <li><a href='hotel_list.html'>Hotels standard list</a></li>
                                        <li><a href='hotel_simple_style.html'>Hotels simple style</a></li>
                                        <li><a href='hotel_detail_style.html'>Hotels detail style</a></li>
                                        <li><a href='hotel_detail.html'>Hotel item page</a></li>
                                        <li><a href='hotel_booking.html'>Hotel booking page</a></li>
                                        <li><a href='booking_complete.html'>booking complete page</a></li>
                                    </ul>
                                </li>
                                <li><a href='#'>Flights</a>
                                    <ul>
                                        <li><a href='flight_round_trip.html'>Flights round trip</a></li>
                                        <li><a href='flight_one_way.html'>flights one way trip</a></li>
                                        <li><a href='flight_alternative.html'>flights alternative style</a></li>
                                        <li><a href='flight_detail.html'>Flights detail page</a></li>
                                        <li><a href='flight_booking.html'>Flights booking page</a></li>
                                        <li><a href='booking_complete.html'>booking complete</a></li>
                                    </ul>
                                </li>
                                <li><a href='#'>Tours</a>
                                    <ul>
                                        <li><a href='tour_alternative.html'>Tours list style</a></li>
                                        <li><a href='tour_grid.html'>tours grid style</a></li>
                                        <li><a href='tour_simple.html'>Tours simple style</a></li>
                                        <li><a href='tour_detail.html'>Tour detail page</a></li>
                                        <li><a href='tour_booking.html'>tour booking page</a></li>
                                        <li><a href='booking_complete.html'>booking complete</a></li>
                                    </ul>
                                </li>
                                <li><a href='#'>Pages</a>
                                    <ul>
                                        <li><a href='about_us.html'>about us style one</a></li>
                                        <li><a href='services.html'>services</a></li>
                                        <li><a href='contacts.html'>contact us</a></li>
                                    </ul>
                                </li>
                                <li><a href='#'>Portfolio</a>
                                    <ul>
                                        <li><a href='portfolio_three_collumns.html'>Portfolio three columns</a></li>
                                        <li><a href='portfolio_four_collumns.html'>portfolio four columns</a></li>
                                        <li><a href='item_page.html'>item page</a></li>
                                        <li><a href='item_page_full_width.html'>Item page full width style</a></li>
                                    </ul>
                                </li>
                                <li><a href='#'>Blog</a>
                                    <ul>
                                        <li><a href='blog_with_sidebar.html'>Blog with sidebar</a></li>
                                        <li><a href='blog_masonry.html'>blog masonry style</a></li>
                                        <li><a href='standart_blog_post.html'>Blog post example</a></li>
                                    </ul>
                                </li>
                                <li><a href='#'>Features</a>
                                    <ul>
                                        <li><a href='typography.html'>typography</a></li>
                                        <li><a href='shortcodes.html'>shortcodes</a></li>
                                        <li><a href='interactive_elements.html'>interactive elements</a></li>
                                        <li><a href='cover_galleries.html'>cover galleries</a></li>
                                        <li><a href='columns.html'>columns</a></li>
                                    </ul>
                                </li>
                                <li><a href='contacts.html'>Contacts</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class='clear'></div>
                </div>
            </div>";
}

function printSocialMediaEtcHeaderBar()
{
    echo
    "                <div class='header-phone'><span>1 - 555 - 555 - 555</span></div>
                <div class='header-social'>
                    <a href='#' class='social-twitter'></a>
                    <a href='#' class='social-facebook'></a>
                    <a href='#' class='social-vimeo'></a>
                    <a href='#' class='social-pinterest'></a>
                    <a href='#' class='social-instagram'></a>
                </div>
                <div class='header-viewed'>
                    <a href='#' class='header-viewed-btn'>recently viewed</a>
                    <!-- // viewed drop // -->
                    <div class='viewed-drop'>
                        <div class='viewed-drop-a'>
                            <!-- // -->
                            <div class='viewed-item'>
                                <div class='viewed-item-l'>
                                    <a href='#'><img alt='' src='img/v-item-01.jpg' /></a>
                                </div>
                                <div class='viewed-item-r'>
                                    <div class='viewed-item-lbl'><a href='#'>Andrassy Thai Hotel</a></div>
                                    <div class='viewed-item-cat'>location: thailand</div>
                                    <div class='viewed-price'>152$</div>
                                </div>
                                <div class='clear'></div>
                            </div>
                            <!-- \\ -->
                            <!-- // -->
                            <div class='viewed-item'>
                                <div class='viewed-item-l'>
                                    <a href='#'><img alt='' src='img/v-item-02.jpg' /></a>
                                </div>
                                <div class='viewed-item-r'>
                                    <div class='viewed-item-lbl'><a href='#'>Ermin's Hotel</a></div>
                                    <div class='viewed-item-cat'>location: dubai</div>
                                    <div class='viewed-price'>300$</div>
                                </div>
                                <div class='clear'></div>
                            </div>
                            <!-- \\ -->
                            <!-- // -->
                            <div class='viewed-item'>
                                <div class='viewed-item-l'>
                                    <a href='#'><img alt='' src='img/v-item-03.jpg' /></a>
                                </div>
                                <div class='viewed-item-r'>
                                    <div class='viewed-item-lbl'><a href='#'>Best Western Hotel Reither</a></div>
                                    <div class='viewed-item-cat'>location: berlin</div>
                                    <div class='viewed-price'>2300$</div>
                                </div>
                                <div class='clear'></div>
                            </div>
                            <!-- \\ -->
                        </div>
                    </div>
                    <!-- \\ viewed drop \\ -->
                </div>
                <div class='header-lang'>
                    <a href='#'><img alt='' src='img/en.gif' /></a>
                        <div class='langs-drop'>
                            <div><a href='#' class='langs-item en'>english</a></div>
                            <div><a href='#' class='langs-item fr'>francais</a></div>
                            <div><a href='#' class='langs-item de'>deutsch</a></div>
                            <div><a href='#' class='langs-item it'>italiano</a></div>
                        </div>
                    </div>
                    <div class='header-curency'>
                        <a href='#'>USD</a>
                        <div class='curency-drop'>
                            <div><a href='#'>usd</a></div>
                            <div><a href='#'>Eur</a></div>
                            <div><a href='#'>GBR</a></div>
                        </div>
                    </div>
                    <div class='clear'></div>
                </div>";
}
?>
