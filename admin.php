<?php
require "connect.php";
require "flightFunctions.php";

echoHead();
echo "<body>";
echoHeaderA();
$navArray = array();
echoHeaderB($navArray);
echo "<div class='content'>";

if($_POST['viewFlights'] || $_POST['cancelAddFlight'] || $_POST['cancelEditFlight']){
	viewFlights();
}
elseif($_POST['viewAircraft'] || $_POST['cancelAddAircraft']){
	viewAircraft();
}
elseif($_POST['addFlightForm']){
	addFlightForm();
}
elseif($_POST['addAircraftForm']){
	addAircraftForm();
}
elseif($_POST['addFlight']){
	checkFlight();
}
elseif($_POST['addAircraft']){
	checkAircraft();
}
elseif($_POST['editFlight']){
	$flightID = key($_POST['editFlight']);
	$connection = connect();
	$flight = getFlight($connection, $flightID);
	$depID = $flight['departureID'];
	$depCode = getDepartureAirportCode($connection, $flightID);
	$arrID = $flight['arrivalID'];
	$arrCode = getArrivalAirportCode($connection, $flightID);
	$connection->close();
	preg_match_all("/(\d{4})-(\d{2})-(\d{2})/", $flight['departureDate'], $depDateMatches);
	preg_match_all("/(\d{2}):(\d{2}):(\d{2})/", $flight['departureTime'], $depTimeMatches);
	preg_match_all("/(\d{4})-(\d{2})-(\d{2})/", $flight['arrivalDate'], $arrDateMatches);
	preg_match_all("/(\d{2}):(\d{2}):(\d{2})/", $flight['arrivalTime'], $arrTimeMatches);
	$depYear = $depDateMatches[1][0];
	$depMonth = $depDateMatches[2][0];
	$depDay = $depDateMatches[3][0];
	$depHour = $depTimeMatches[1][0];
	$depMinute = $depTimeMatches[2][0];
	$arrYear = $arrDateMatches[1][0];
	$arrMonth = $arrDateMatches[2][0];
	$arrDay = $arrDateMatches[3][0];
	$arrHour = $arrTimeMatches[1][0];
	$arrMinute = $arrTimeMatches[2][0];
	editFlightForm($flightID, $flight['flightNumber'], $depID, $depYear, $depMonth, $depDay, $depHour, $depMinute, $depCode, 
		$arrID, $arrYear, $arrMonth, $arrDay, $arrHour, $arrMinute, $arrCode, $flight['aircraftRegistration'], $flight['price']);
}
elseif($_POST['edit']){
	checkEditFlight();
}
elseif($_POST['removeFlight']){
	removeFlight(key($_POST['removeFlight']));
}
elseif($_POST['removeAircraft']){
	removeAircraft(key($_POST['removeAircraft']));
}
else{
	viewAdminPanel();
}

echo "</div>";
echoFooterB();
echo "</body>";

// Displays the administrator panel
function viewAdminPanel(){
	echo "<div class='heading'>Administrator Panel</div>".
			"<form method='post'>".
			"<div class='panel'>".
			"<div class='pane'>".
			"<div class='pane-image'>".
			"<img src='display.png' alt='' class='fade'>".
			"</div>".
			"<div class='pane-text'>".
			"<input type='submit' name='viewFlights' value='View Flights' class='invisible-button'>".
			"</div>".
			"</div>".
			"<div class='pane'>".
			"<div class='pane-image'>".
			"<img src='Boeing737.png' alt='' class='fade'>".
			"</div>".
			"<div class='pane-text'>".
			"<input type='submit' name='viewAircraft' value='View Aircraft' class='invisible-button'>".
			"</div>".
			"</div>".
			"</div>".
			"</form>";
}

// Displays all flights
function viewFlights(){
	echo "<div class='heading'>Flights</div>".
			"<form method='post'>".
			"<input type='submit' class='button' name='addFlightForm' value='Add Flight'>";
	$connection = connect();
	$result = getFlights($connection);
	if($result->num_rows > 0){
		echo "<table class='table'>".
				"<tr>".
				"<th>Flight</th>".
				"<th>Dep. Date</th>".
				"<th>Dep. Time</th>".
				"<th>Dep. Airport</th>".
				"<th>Arr. Date</th>".
				"<th>Arr. Time</th>".
				"<th>Arr. Airport</th>".
				"<th>Aircraft</th>".
				"<th>Price</th>".
				"<th></th>".
				"<th></th>".
				"</tr>";
		while($row = $result->fetch_assoc()){
			echo "<tr>".
					"<td>".$row['flightNumber']."</td>".
					"<td>".$row['departureDate']."</td>".
					"<td>".$row['departureTime']."</td>".
					"<td>".getDepartureAirportCode($connection, $row['flightID'])."</td>".
					"<td>".$row['arrivalDate']."</td>".
					"<td>".$row['arrivalTime']."</td>".
					"<td>".getArrivalAirportCode($connection, $row['flightID'])."</td>".
					"<td>".$row['aircraftRegistration']."</td>".
					"<td>$".$row['price']."</td>".
					"<td><input type='submit' class='button' name='editFlight[".$row['flightID']."]' value='Edit'></td>".
					"<td><input type='submit' class='button' name='removeFlight[".$row['flightID']."]' value='Remove'></td>".
					"</tr>";
		}
		echo "</table>".
				"</form>";
	}
	else{
		echo "<div class='subheading'></div>".
				"No flights were found.";
	}
	$connection->close();
}

// Displays all aircraft
function viewAircraft(){
	echo "<div class='heading'>Aircraft</div>".
			"<form method='post'>".
			"<input type='submit' class='button' name='addAircraftForm' value='Add Aircraft'>";
	$connection = connect();
	$result = getAircraft($connection);
	if($result->num_rows > 0){
		echo "<table class='table'>".
				"<tr>".
				"<th>Registration</th>".
				"<th>Model</th>".
				"<th></th>".
				"</tr>".
				"<form method='post'>";
		while($row = $result->fetch_assoc()){
			echo "<tr>".
					"<td>".$row['aircraftRegistration']."</td>".
					"<td>".$row['modelName']."</td>".
					"<td><input type='submit' class='button' name='removeAircraft[".$row['aircraftID']."]' value='Remove'></td>".
					"</tr>";
		}
		echo "</table>".
				"</form>";
	}
	else{
		echo "<div class='subheading'></div>".
				"No aircraft were found.";
	}
	$connection->close();
}

// Displays the add flight form
function addFlightForm($flightNumber, $depYear = "", $depMonth = "", $depDay = "", $depHour = "", $depMinute = "", $depCode = "",
		$arrYear = "", $arrMonth = "", $arrDay = "", $arrHour = "", $arrMinute = "", $arrCode = "", $aircraftRegistration = "", $price = ""){
	echo "<div class='heading'>Add Flight</div>".
			"<form method='post'>".
			"<div class='form-row'>".
			"<div class='title'>Flight Number:</div>".
			"<div class='field'><input type='text' name='flightNumber' value='".$flightNumber."'></div>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Dep. Date:</div>".
			"<select class='custom-select' name='depYear'>".
			"<option>Year</option>";
	for($i = 2016; $i < 2018; $i++){
		$year = $i;
		echo "<option value='".$year."'";
		if($depYear == $year){
			echo " selected='selected'";
		}
		echo ">".$year."</option>";
	}
	echo "</select>".
			"<div class='space'></div>".
			"<select class='custom-select' name='depMonth'>".
			"<option>Month</option>";
	for($i = 1; $i < 13; $i++){
		if($i < 10){
			$month = "0".$i;
		}
		else{
			$month = $i;
		}
		echo "<option value='".$month."'";
		if($depMonth == $month){
			echo " selected='selected'";
		}
		echo ">".$month."</option>";
	}
	echo "</select>".
			"<div class='space'></div>".
			"<select class='custom-select' name='depDay'>".
			"<option>Day</option>";
	for($i = 1; $i < 32; $i++){
		if($i < 10){
			$day = "0".$i;
		}
		else{
			$day = $i;
		}
		echo "<option value='".$day."'";
		if($depDay == $day){
			echo " selected='selected'";
		}
		echo ">".$day."</option>";
	}
	echo "</select>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Dep. Time:</div>".
			"<select class='custom-select' name='depHour'>".
			"<option>Hour</option>";
	for($i = 0; $i < 24; $i++){
		if($i < 10){
			$hour = "0".$i;
		}
		else{
			$hour = $i;
		}
		echo "<option value='".$hour."'";
		if($depHour == $hour){
			echo " selected='selected'";
		}
		echo ">".$hour."</option>";
	}
	echo "</select>".
			"<div class='space'></div>".
			"<select class='custom-select' name='depMinute'>".
			"<option>Minute</option>";
	for($i = 0; $i < 60; $i++){
		if($i < 10){
			$minute = "0".$i;
		}
		else{
			$minute = $i;
		}
		echo "<option value='".$minute."'";
		if($depMinute == $minute){
			echo " selected='selected'";
		}
		echo ">".$minute."</option>";
	}
	echo "</select>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Dep. Airport Code:</div>".
			"<div class='field'><input type='text' name='depCode' value='".$depCode."'></div>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Arr. Date:</div>".
			"<select class='custom-select' name='arrYear'>".
			"<option>Year</option>";
	for($i = 2016; $i < 2018; $i++){
		$year = $i;
		echo "<option value='".$year."'";
		if($arrYear == $year){
			echo " selected='selected'";
		}
		echo ">".$year."</option>";
	}
	echo "</select>".
			"<div class='space'></div>".
			"<select class='custom-select' name='arrMonth'>".
			"<option>Month</option>";
	for($i = 1; $i < 13; $i++){
		if($i < 10){
			$month = "0".$i;
		}
		else{
			$month = $i;
		}
		echo "<option value='".$month."'";
		if($arrMonth == $month){
			echo " selected='selected'";
		}
		echo ">".$month."</option>";
	}
	echo "</select>".
			"<div class='space'></div>".
			"<select class='custom-select' name='arrDay'>".
			"<option>Day</option>";
	for($i = 1; $i < 32; $i++){
		if($i < 10){
			$day = "0".$i;
		}
		else{
			$day = $i;
		}
		echo "<option value='".$day."'";
		if($arrDay == $day){
			echo " selected='selected'";
		}
		echo ">".$day."</option>";
	}
	echo "</select>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Arr. Time:</div>".
			"<select class='custom-select' name='arrHour'>".
			"<option>Hour</option>";
	for($i = 0; $i < 24; $i++){
		if($i < 10){
			$hour = "0".$i;
		}
		else{
			$hour = $i;
		}
		echo "<option value='".$hour."'";
		if($arrHour == $hour){
			echo " selected='selected'";
		}
		echo ">".$hour."</option>";
	}
	echo "</select>".
			"<div class='space'></div>".
			"<select class='custom-select' name='arrMinute'>".
			"<option>Minute</option>";
	for($i = 0; $i < 60; $i++){
		if($i < 10){
			$minute = "0".$i;
		}
		else{
			$minute = $i;
		}
		echo "<option value='".$minute."'";
		if($arrMinute == $minute){
			echo " selected='selected'";
		}
		echo ">".$minute."</option>";
	}
	echo "</select>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Arr. Airport Code:</div>".
			"<div class='field'><input type='text' name='arrCode' value='".$arrCode."'></div>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Aircraft Registration:</div>".
			"<div class='field'><input type='text' name='aircraftRegistration' value='".$aircraftRegistration."'></div>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Price: $</div>".
			"<div class='field'><input type='text' name='price' value='".$price."'></div>".
			"</div>".
			"<input type='submit' class='button' name='addFlight' value='Add'>".
			"<div class='space'></div>".
			"<input type='submit' class='button' name='cancelAddFlight' value='Cancel'>".
			"</form>";
}

// Displays the add aircraft form
function addAircraftForm($registration){
	echo "<div class='heading'>Add Aircraft</div>".
			"<form method='post'>".
			"<div class='form-row'>".
			"<div class='title'>Aircraft Registration:</div>".
			"<div class='field'><input type='text' name='aircraftRegistration' value='".$registration."'></div>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Model:</div>".
			"<select class='custom-select' name='modelID'>";
	$connection = connect();
	$result = getAircraftModels($connection);
	while($row = $result->fetch_assoc()){
		echo "<option value='".$row['modelID']."'>".$row['modelName']."</option>";
	}
	$connection->close();
	echo "</select>".
			"</div>".
			"<input type='submit' class='button' name='addAircraft' value='Add'>".
			"<div class='space'></div>".
			"<input type='submit' class='button' name='cancelAddAircraft' value='Cancel'>".
			"</form>";
}

// Displays the edit flight form
function editFlightForm($flightID, $flightNumber, $departureID, $depYear, $depMonth, $depDay, $depHour, $depMinute, $depCode,
		$arrivalID, $arrYear, $arrMonth, $arrDay, $arrHour, $arrMinute, $arrCode, $aircraftRegistration, $price){
	echo "<form method='post'>".
			"<input type='hidden' name='depID' value='".$departureID."'>".
			"<input type='hidden' name='arrID' value='".$arrivalID."'>".
			"<input type='hidden' name='flightID' value='".$flightID."'>".
			"<div class='heading'>Edit Flight</div>".
			"<div class='form-row'>".
			"<div class='title'>Flight Number:</div>".
			"<div class='field'><input type='text' name='flightNumber' value='".$flightNumber."'></div>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Dep. Date:</div>".
			"<select class='custom-select' name='depYear'>".
			"<option>Year</option>";
	for($i = 2016; $i < 2018; $i++){
		$year = $i;
		echo "<option value='".$year."'";
		if($depYear == $year){
			echo " selected='selected'";
		}
		echo ">".$year."</option>";
	}
	echo "</select>".
			"<div class='space'></div>".
			"<select class='custom-select' name='depMonth'>".
			"<option>Month</option>";
	for($i = 1; $i < 13; $i++){
		if($i < 10){
			$month = "0".$i;
		}
		else{
			$month = $i;
		}
		echo "<option value='".$month."'";
		if($depMonth == $month){
			echo " selected='selected'";
		}
		echo ">".$month."</option>";
	}
	echo "</select>".
			"<div class='space'></div>".
			"<select class='custom-select' name='depDay'>".
			"<option>Day</option>";
	for($i = 1; $i < 32; $i++){
		if($i < 10){
			$day = "0".$i;
		}
		else{
			$day = $i;
		}
		echo "<option value='".$day."'";
		if($depDay == $day){
			echo " selected='selected'";
		}
		echo ">".$day."</option>";
	}
	echo "</select>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Dep. Time:</div>".
			"<select class='custom-select' name='depHour'>".
			"<option>Hour</option>";
	for($i = 0; $i < 24; $i++){
		if($i < 10){
			$hour = "0".$i;
		}
		else{
			$hour = $i;
		}
		echo "<option value='".$hour."'";
		if($depHour == $hour){
			echo " selected='selected'";
		}
		echo ">".$hour."</option>";
	}
	echo "</select>".
			"<div class='space'></div>".
			"<select class='custom-select' name='depMinute'>".
			"<option>Minute</option>";
	for($i = 0; $i < 60; $i++){
		if($i < 10){
			$minute = "0".$i;
		}
		else{
			$minute = $i;
		}
		echo "<option value='".$minute."'";
		if($depMinute == $minute){
			echo " selected='selected'";
		}
		echo ">".$minute."</option>";
	}
	echo "</select>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Dep. Airport Code:</div>".
			"<div class='field'><input type='text' name='depCode' value='".$depCode."'></div>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Arr. Date:</div>".
			"<select class='custom-select' name='arrYear'>".
			"<option>Year</option>";
	for($i = 2016; $i < 2018; $i++){
		$year = $i;
		echo "<option value='".$year."'";
		if($arrYear == $year){
			echo " selected='selected'";
		}
		echo ">".$year."</option>";
	}
	echo "</select>".
			"<div class='space'></div>".
			"<select class='custom-select' name='arrMonth'>".
			"<option>Month</option>";
	for($i = 1; $i < 13; $i++){
		if($i < 10){
			$month = "0".$i;
		}
		else{
			$month = $i;
		}
		echo "<option value='".$month."'";
		if($arrMonth == $month){
			echo " selected='selected'";
		}
		echo ">".$month."</option>";
	}
	echo "</select>".
			"<div class='space'></div>".
			"<select class='custom-select' name='arrDay'>".
			"<option>Day</option>";
	for($i = 1; $i < 32; $i++){
		if($i < 10){
			$day = "0".$i;
		}
		else{
			$day = $i;
		}
		echo "<option value='".$day."'";
		if($arrDay == $day){
			echo " selected='selected'";
		}
		echo ">".$day."</option>";
	}
	echo "</select>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Arr. Time:</div>".
			"<select class='custom-select' name='arrHour'>".
			"<option>Hour</option>";
	for($i = 0; $i < 24; $i++){
		if($i < 10){
			$hour = "0".$i;
		}
		else{
			$hour = $i;
		}
		echo "<option value='".$hour."'";
		if($arrHour == $hour){
			echo " selected='selected'";
		}
		echo ">".$hour."</option>";
	}
	echo "</select>".
			"<div class='space'></div>".
			"<select class='custom-select' name='arrMinute'>".
			"<option>Minute</option>";
	for($i = 0; $i < 60; $i++){
		if($i < 10){
			$minute = "0".$i;
		}
		else{
			$minute = $i;
		}
		echo "<option value='".$minute."'";
		if($arrMinute == $minute){
			echo " selected='selected'";
		}
		echo ">".$minute."</option>";
	}
	echo "</select>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Arr. Airport Code:</div>".
			"<div class='field'><input type='text' name='arrCode' value='".$arrCode."'></div>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Aircraft Registration:</div>".
			"<div class='field'><input type='text' name='aircraftRegistration' value='".$aircraftRegistration."'></div>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Price: $</div>".
			"<div class='field'><input type='text' name='price' value='".$price."'></div>".
			"</div>".
			"<input type='submit' class='button' name='edit' value='Edit'>".
			"<div class='space'></div>".
			"<input type='submit' class='button' name='cancelEditFlight' value='Cancel'>".
			"</form>";
}

// Validates add flight form data
function checkFlight(){
	$flightNumber = trim(htmlentities($_POST['flightNumber'], ENT_QUOTES));
	$depYear = $_POST['depYear'];
	$depMonth = $_POST['depMonth'];
	$depDay = $_POST['depDay'];
	$depHour = $_POST['depHour'];
	$depMinute = $_POST['depMinute'];
	$depCode = trim(htmlentities($_POST['depCode'], ENT_QUOTES));
	$arrYear = $_POST['arrYear'];
	$arrMonth = $_POST['arrMonth'];
	$arrDay = $_POST['arrDay'];
	$arrHour = $_POST['arrHour'];
	$arrMinute = $_POST['arrMinute'];
	$arrCode = trim(htmlentities($_POST['arrCode'], ENT_QUOTES));
	$aircraftRegistration = trim(htmlentities($_POST['aircraftRegistration'], ENT_QUOTES));
	strtoupper($aircraftRegistration);
	$price = trim(htmlentities($_POST['price'], ENT_QUOTES));
	$error = "";
	if(!preg_match_all("/[A-Z\d]{4}/", $flightNumber, $matches)){
		$error = "Please enter a valid flight number.";
	}
	elseif(!preg_match_all("/[A-Z]{4}/", $depCode, $matches) || !preg_match_all("/[A-Z]{4}/", $arrCode, $matches)){
		$error = "Please enter a valid airport code.";
	}
	elseif(!preg_match_all("/N[A-Z\d]{5}/", $aircraftRegistration, $matches)){
		$error = "Please enter a valid aircraft registration.";
	}
	elseif($price < 10 or $price > 99999.99){
		$error = "Please enter a valid price.";
	}
	if($error){
		echo "<font color='red'>".$error."</font>";
		addFlightForm($flightNumber, $depYear, $depMonth, $depDay, $depHour, $depMinute, $depCode,
		$arrYear, $arrMonth, $arrDay, $arrHour, $arrMinute, $arrCode, $aircraftRegistration, $price);
	}
	else{
		$connection = connect();
		$depDate = $depYear."-".$depMonth."-".$depDay;
		$depTime = $depHour.":".$depMinute.":00";
		$depAirportID = getAirportID($connection, $depCode);
		$depCityID = getCityID($connection, $depAirportID);
		$arrDate = $arrYear."-".$arrMonth."-".$arrDay;
		$arrTime = $arrHour.":".$arrMinute.":00";
		$arrAirportID = getAirportID($connection, $arrCode);
		$arrCityID = getCityID($connection, $arrAirportID);
		insertDeparture($connection, $depDate, $depTime, $depAirportID, $depCityID);
		$departureID = mysqli_insert_id($connection);
		insertArrival($connection, $arrDate, $arrTime, $arrAirportID, $arrCityID);
		$arrivalID = mysqli_insert_id($connection);
		$aircraftID = getAircraftID($connection, $aircraftRegistration);
		insertFlight($connection, $flightNumber, $price, $aircraftID, $departureID, $arrivalID);
		$connection->close();
		viewFlights();
	}
}

// Validates edit flight form data
function checkEditFlight(){
	$departureID = $_POST['depID'];
	$arrivalID = $_POST['arrID'];
	$flightID = $_POST['flightID'];
	$flightNumber = trim(htmlentities($_POST['flightNumber'], ENT_QUOTES));
	$depYear = $_POST['depYear'];
	$depMonth = $_POST['depMonth'];
	$depDay = $_POST['depDay'];
	$depHour = $_POST['depHour'];
	$depMinute = $_POST['depMinute'];
	$depCode = trim(htmlentities($_POST['depCode'], ENT_QUOTES));
	$arrYear = $_POST['arrYear'];
	$arrMonth = $_POST['arrMonth'];
	$arrDay = $_POST['arrDay'];
	$arrHour = $_POST['arrHour'];
	$arrMinute = $_POST['arrMinute'];
	$arrCode = trim(htmlentities($_POST['arrCode'], ENT_QUOTES));
	$aircraftRegistration = trim(htmlentities($_POST['aircraftRegistration'], ENT_QUOTES));
	strtoupper($aircraftRegistration);
	$price = trim(htmlentities($_POST['price'], ENT_QUOTES));
	$error = "";
	if(!preg_match_all("/[A-Z\d]{4}/", $flightNumber, $matches)){
		$error = "Please enter a valid flight number.";
	}
	elseif(!preg_match_all("/[A-Z]{4}/", $depCode, $matches) || !preg_match_all("/[A-Z]{4}/", $arrCode, $matches)){
		$error = "Please enter a valid airport code.";
	}
	elseif(!preg_match_all("/N[A-Z\d]{5}/", $aircraftRegistration, $matches)){
		$error = "Please enter a valid aircraft registration.";
	}
	elseif($price < 10 or $price > 99999.99){
		$error = "Please enter a valid price.";
	}
	if($error){
		echo "<font color='red'>".$error."</font>";
		editFlightForm($flightID, $flightNumber, $departureID, $depYear, $depMonth, $depDay, $depHour, $depMinute, $depCode, 
			$arrivalID, $arrYear, $arrMonth, $arrDay, $arrHour, $arrMinute, $arrCode, $aircraftRegistration, $price);
	}
	else{
		$connection = connect();
		$depDate = $depYear."-".$depMonth."-".$depDay;
		$depTime = $depHour.":".$depMinute.":00";
		$depAirportID = getAirportID($connection, $depCode);
		$depCityID = getCityID($connection, $depAirportID);
		$arrDate = $arrYear."-".$arrMonth."-".$arrDay;
		$arrTime = $arrHour.":".$arrMinute.":00";
		$arrAirportID = getAirportID($connection, $arrCode);
		$arrCityID = getCityID($connection, $arrAirportID);
		$aircraftID = getAircraftID($connection, $aircraftRegistration);
		updateDeparture($connection, $departureID, $depDate, $depTime, $depAirportID, $depCityID);
		updateArrival($connection, $arrivalID, $arrDate, $arrTime, $arrAirportID, $arrCityID);
		updateFlight($connection, $flightID, $flightNumber, $price, $aircraftID, $departureID, $arrivalID);
		$connection->close();
		viewFlights();
	}
}

// Validates add aircraft form data
function checkAircraft(){
	$registration = trim(htmlentities($_POST['aircraftRegistration'], ENT_QUOTES));
	strtoupper($registration);
	$modelID = $_POST['modelID'];
	if(!preg_match_all("/N[A-Z\d]{5}/", $registration, $matches)){
		$error = "Please enter a valid aircraft registration.";
	}
	if($error){
		echo "<font color='red'>".$error."</font>";
		addAircraftForm($registration);
	}
	else{
		$connection = connect();
		insertAircraft($connection, $registration, $modelID);
		$connection->close();
		viewAircraft();
	}
}

// Deletes a flight from the flight database
function removeFlight($flightID){
	$connection = connect();
	deleteFlight($connection, $flightID);
	$connection->close();
	viewFlights();
}

// Deletes an aircraft from the flight database
function removeAircraft($aircraftID){
	$connection = connect();
	deleteAircraft($connection, $aircraftID);
	$connection->close();
	viewAircraft();
}
?>