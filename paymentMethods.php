<?php
require "connect.php";
require "flightFunctions.php";

echoHead();

session_start();

echo "<body>";
echoHeaderA();
$navArray = array(
		"<a href='http://localhost/test/flightbooking/paymentMethods.php'>Payment Methods</a>",
		"<a href='http://localhost/test/flightbooking/paymentHistory.php'>Payment History</a>",
		"<a href='http://localhost/test/flightbooking/flightHistory.php'>Flight History</a>"
);
echoHeaderB($navArray);
echo "<div class='content'>";

if($_POST['addPaymentMethodForm']){
	addPaymentMethodForm();
}
elseif($_POST['addPaymentMethod']){
	checkPaymentMethod();
}
elseif($_POST['removePaymentMethod']){
	removePaymentMethod(key($_POST['removePaymentMethod']));
}
else{
	viewPaymentMethods();	
}

echo "</div>";
echoFooterB();
echo "</body>";

// Displays the payment methods page
function viewPaymentMethods(){
	echo "<div class='heading'>Payment Methods</div>".
			"<form method='post'>".
			"<input type='submit' class='button' name='addPaymentMethodForm' value='Add Payment Method'>".
			"</form>".
			"<div class='subheading'></div>";
	$connection = connect();
	$result = getUserPaymentMethods($connection, $_SESSION["userID"]);
	if($result->num_rows > 0){
		echo "<table class='invisibleTable'>".
				"<form method='post'>";
		while($row = $result->fetch_assoc()){
			echo "<tr>".
					"<td>".$row['paymentTypeName']."</td>";
			preg_match_all("/\d{12}(\d{4})/", $row['cardNumber'], $matches);
			echo "<td>XXXX-XXXX-XXXX-".$matches[1][0]."</td>".
					"<td><input type='submit' class='button' name='removePaymentMethod[".$row['paymentMethodID']."]' value='Remove'></td>".
					"</tr>";
		}
		echo "</form>".
				"</table>";
	}
	else{
		echo "No payment methods are associated with this account.";
	}
	$connection->close();
}

// Displays the add payment method form
function addPaymentMethodForm($cardType = "", $cardNumber = "", $cardHolder = "", $expirationYear = "", $expirationMonth = "", $securityCode = ""){
	echo "<div class='heading'>Add Credit or Debit Card</div>".
			"<form method='post'>".
			"<div class='form-row'>".
			"<div class='title'>Card Type:</div>".
			"<select class='custom-select' name='cardType'>";
	$connection = connect();
	$result = getPaymentTypes($connection);
	while($row = $result->fetch_assoc()){
		echo "<option value='".$row['paymentTypeID']."'>".$row['paymentTypeName']."</option>";
	}
	$connection->close();
	echo "</select>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Card Number:</div>".
			"<div class='field'><input type='text' name='cardNumber' value='".$cardNumber."'></div>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Card Holder:</div>".
			"<div class='field'><input type='text' name='cardHolder' value='".$cardHolder."'></div>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Expiration Date:</div>".
			"<select class='custom-select' name='expirationYear' value='".$expirationYear."'>".
			"<option>Year</option>".
			"<option value='2016'>2016</option>".
			"<option value='2017'>2017</option>".
			"<option value='2018'>2018</option>".
			"<option value='2019'>2019</option>".
			"<option value='2020'>2020</option>".
			"</select>".
			"<div class='space'></div>".
			"<select class='custom-select' name='expirationMonth' value='".$expirationMonth."'>".
			"<option>Month</option>".
			"<option value='01'>01</option>".
			"<option value='02'>02</option>".
			"<option value='03'>03</option>".
			"<option value='04'>04</option>".
			"<option value='05'>05</option>".
			"<option value='06'>06</option>".
			"<option value='07'>07</option>".
			"<option value='08'>08</option>".
			"<option value='09'>09</option>".
			"<option value='10'>10</option>".
			"<option value='11'>11</option>".
			"<option value='12'>12</option>".
			"</select>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Security Code:</div>".
			"<div class='field'><input type='text' name='securityCode' value='".$securityCode."'></div>".
			"</div>".
			"<input type='submit' class='button' name='addPaymentMethod' value='Add'>".
			"<div class='space'></div>".
			"<input type='submit' class='button' name='cancel' value='Cancel'>".
			"</form>";
}

// Validates add payment method form data
function checkPaymentMethod(){
	$cardType = $_POST['cardType'];
	$cardNumber = trim(htmlentities($_POST['cardNumber'], ENT_QUOTES));
	$cardHolder = trim(htmlentities($_POST['cardHolder'], ENT_QUOTES));
	$expirationYear = $_POST['expirationYear'];
	$expirationMonth = $_POST['expirationMonth'];
	$securityCode = trim(htmlentities($_POST['securityCode'], ENT_QUOTES));
	$error = "";
	if(!preg_match_all("/\d{16}/", $cardNumber, $matches)){
		$error = "Please enter a valid card number";
	}
	elseif(!preg_match_all("/[A-Za-z]+/", $cardHolder, $matches)){
		$error = "Please enter a valid name.";
	}
	elseif(!preg_match_all("/\d{3}\d?/", $securityCode, $matches)){
		$error = "Please enter a valid security code.";
	}
	if($error){
		echo "<font color='red'>".$error."</font>";
		addPaymentMethodForm($cardType, $cardNumber, $cardHolder, $expirationYear, $expirationMonth, $securityCode);
	}
	else{
		$connection = connect();
		insertPaymentMethod($connection, $cardType, $cardNumber, $cardHolder, $expirationYear, $expirationMonth, $securityCode, $_SESSION["userID"]);
		$connection->close();
		viewPaymentMethods();
	}
}

// Removes a payment method from the flight database
function removePaymentMethod($paymentMethodID){
	$connection = connect();
	deletePaymentMethod($connection, $paymentMethodID);
	$connection->close();
	viewPaymentMethods();
}
?>