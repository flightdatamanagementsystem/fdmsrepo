<?php
//session_start();
/* Kristin Hamilton
 * created 04-Apr-2016
 * last modified 26-Apr-2016
 */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

class PricingImpl implements Pricing
{
    function __construct($booking)
    {
        $this->flight1 = $booking->getFlight1();
        $this->flight2 = $booking->getFlight2();
        //$this->seatSelection = $booking->getSeatSelection();
    }

    function getBasePrice()
    {
        $flight1Price = $this->flight1->getFlightPrice();
        $flight1TravelerCount = $this->flight1->getFlightTravelerCount();
        $flight1Total = $flight1Price * $flight1TravelerCount;
        
        $flight2Price = $this->flight2->getFlightPrice();
        $flight2TravelerCount = $this->flight2->getFlightTravelerCount();
        $flight2Total = $flight2Price * $flight2TravelerCount;
        
        return $flight1Total + $flight2Total;
    }
    
    // Returns float taxes+fees price
    function getTaxesAndFees()
    {        
        // TODO: need to go through each seat for flight and get base price for each ticket
        $basePrice = $this->getBasePrice();  
        $feeRate = $this->getFeeRate();
        
        $salesTaxRate = 0.0825;
        $fees = $basePrice * $feeRate;        // calculate fees for basePrice only
        $taxes = $salesTaxRate * $basePrice;  // calculate tax for basePrice (not including fees)
        return $taxes + $fees;
    }
    
    // returns float (percent / 100)
    // example: 1% fee rate determined => returns 0.01
    function getFeeRate()
    {
        $diffInSeconds = 0;
        $purchaseDatetime = '';
        $flightDepartDate = $this->flight1->getFlightDepartureDate(false);
        $flightDepartTime = $this->flight1->getFlightDepartureTime();
                
        $query = "SELECT NOW();";
        $db = dbConnect();
        $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain current datetime<br />");
        if($result) $purchaseDatetime = mysqli_fetch_row($result)[0];
        
        $query = 
        "SELECT TIMEDIFF('$purchaseDatetime', '$flightDepartDate $flightDepartTime') 
        * 24 * 60 * 60;";
        $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain timediff<br />");
        if($result) $diffInSeconds = mysqli_fetch_row($result)[0];
        mysqli_close($db);

        $feeRate = 0.0005;  // initial rate every customer is to pay 
        
        $twelveHoursInSeconds = 60 * 60 *12;
        $twentyFourHoursInSeconds = $twelveHoursInSeconds * 2;
        $fortyEightHoursInSeconds = $twentyFourHoursInSeconds * 2;
        $oneWeekInSeconds = $twentyFourHoursInSeconds * 7;
        $twoWeeksInSeconds = $oneWeekInSeconds * 2;
        $threeWeeksInSeconds = $oneWeekInSeconds * 3;

        if($diffInSeconds <= $twoWeeksInSeconds){ $feeRate *= 2; }
        if($diffInSeconds <= $oneWeekInSeconds){ $feeRate *= 2.5; }
        if($diffInSeconds <= $fortyEightHoursInSeconds){ $feeRate *= 3; }
        if($diffInSeconds <= $twentyFourHoursInSeconds){ $feeRate *= 4; }
        if($diffInSeconds <= $twelveHoursInSeconds){ $feeRate *= 5; }
        
        return $feeRate;
    }
}
?>